package bred_soft.stop_player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import bred_soft.stop_player.player.StopPlayer;

import static bred_soft.stop_player.storage.MusicStorage.Playlist;
import static bred_soft.stop_player.ListOfPlaylistsAdapter.ClickListener;
import static bred_soft.utils.AutoBindService.getService;



@SuppressWarnings("WeakerAccess")
public class ListOfPlaylistsActivity extends Activity
    implements ServiceConnection, ClickListener
{
    public SearchView menu_playlist_search = null;
    public ListView list_of_playlists;
    
    protected StopPlayer SP = null;
    protected ListOfPlaylistsAdapter lofpl_adapter = null;
    protected int selected_playlist = -1;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState_)
    {
        super.onCreate(savedInstanceState_);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.list_of_playlists);
        
        list_of_playlists = (ListView)findViewById(R.id.list_of_playlists);
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        bindService(new Intent(this, StopPlayer.class), this, BIND_AUTO_CREATE);
    }
    
    @Override
    protected void onPause()
    {
        onServiceDisconnected(null); unbindService(this);
        super.onPause();
    }
    
    @Override
    protected void onDestroy()
    {
        list_of_playlists.setOnItemClickListener(null);
        list_of_playlists = null;
        menu_playlist_search = null;
        super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu_)
    {
        getMenuInflater().inflate(R.menu.list_of_playlists, menu_);
        
        menu_playlist_search = (SearchView)menu_.findItem(R.id.menu_playlist_search)
                                                .getActionView();
        if(lofpl_adapter != null)
            menu_playlist_search.setOnQueryTextListener(lofpl_adapter);
        
        return super.onCreateOptionsMenu(menu_);
    }
    
    @Override
    public void onServiceConnected(ComponentName name_, IBinder binder_)
    {
        SP = getService(binder_);
        if(lofpl_adapter != null) lofpl_adapter.destroy();
        
        lofpl_adapter = new ListOfPlaylistsAdapter(
            this, this, list_of_playlists, SP.Storage
        );
        if(menu_playlist_search != null)
            menu_playlist_search.setOnQueryTextListener(lofpl_adapter);
    }
    
    @Override
    public void onServiceDisconnected(ComponentName name_)
    {
        SP = null;
        if(lofpl_adapter != null)
        {
            if(menu_playlist_search != null)
                menu_playlist_search.setOnQueryTextListener(null);
            
            lofpl_adapter.destroy(); lofpl_adapter = null;
        }
    }
    
    // ---
    
    @Override
    public void onItemClick(long id_)
    {
        selected_playlist = -1;
        startActivity(new Intent(this, PlaylistActivity.class)
                      .putExtra("playlist-id", (int)id_));
    }
    
    @Override
    public void onItemDoc(long id_)
    {
        selected_playlist = (int)id_;
        
        new AlertDialog.Builder(this)
            .setCancelable(true)
            .setTitle(SP.Storage.getPlaylistName(selected_playlist))
            .setItems(R.array.playlist_dialog, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                dialog_.dismiss();
                switch(which_)
                {
                case 0: onClickPlay();   return;
                case 1: onClickPaste();  return;
                case 2: onClickRename(); return;
                case 3: onClickCopy();   return;
                case 4: onClickMerge();  return;
                case 5: onClickRemove();
                }
            }
        }).show();
    }
    
    
    // --- Actions --- //
    
    public interface _PlaylistNameCallback
    {   void set(String new_name_);   }
    
    public static void _playlistNameDialog(Activity ct_, int title_rc_,
                                           String started_name_,
                                           final _PlaylistNameCallback handler_)
    {
        View v = ct_.getLayoutInflater().inflate(R.layout.playlist_name_dialog, null);
        final EditText edit_playlist_name = (EditText)v.findViewById(R.id.edit_playlist_name);
        
        edit_playlist_name.setText(started_name_);
        
        new AlertDialog.Builder(ct_)
            .setCancelable(true).setTitle(title_rc_).setView(v)
            .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                String new_name = edit_playlist_name.getText().toString();
                handler_.set(new_name);
            }
        }).show();
    }
    
    // ---
    
    public void onMenuAdd(MenuItem m)
    {
        selected_playlist = -1;
        
        _playlistNameDialog(this, R.string.playlist_add, "", new _PlaylistNameCallback()
        {
            @Override public void set(String new_name_)
            {
                int pl_id = SP.Storage.addPlaylist(new_name_);
                if(pl_id < 0)
                    Toast.makeText(ListOfPlaylistsActivity.this,
                                   R.string.pl_save_error, Toast.LENGTH_SHORT).show();
                else
                    startActivity(new Intent(ListOfPlaylistsActivity.this, PlaylistActivity.class)
                                  .putExtra("playlist-id", pl_id));
            }
        });
    }
    
    // ---
    
    public void onClickPlay()
    {
        if(SP.start(SP.Storage.getPlaylist(selected_playlist).copy()))
        {
            startActivity(new Intent(this, MainActivity.class)
                          .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
        selected_playlist = -1;
    }
    
    public void onClickPaste()
    {
        if(SP.Storage.clipboardTrack == null)
        {
            Toast.makeText(this, R.string.paste_error, Toast.LENGTH_SHORT).show();
            return;
        }
        
        Playlist pl = SP.Storage.getPlaylist(selected_playlist);
        if(pl != null)
        {
            pl.add(-1, SP.Storage.clipboardTrack.copy());
            if(SP.Storage.setPlaylist(selected_playlist, pl))
                startActivity(new Intent(this, PlaylistActivity.class)
                              .putExtra("playlist-id", selected_playlist));
            else
                Toast.makeText(this, R.string.pl_save_error, Toast.LENGTH_SHORT).show();
        }
        selected_playlist = -1;
    }
    
    public void onClickRename()
    {
        _playlistNameDialog(this, R.string.playlist_rename,
                            SP.Storage.getPlaylistName(selected_playlist),
                            new _PlaylistNameCallback()
        {
            @Override public void set(String new_name_)
            {
                if(SP.Storage.renamePlaylist(selected_playlist, new_name_) >= 0)
                    lofpl_adapter.notifyDataSetChanged();
                else
                    Toast.makeText(ListOfPlaylistsActivity.this,
                                   R.string.pl_name_error, Toast.LENGTH_SHORT).show();
                
                selected_playlist = -1;
            }
        });
    }
    
    public void onClickCopy()
    {
        _playlistNameDialog(this, R.string.playlist_copy,
                            SP.Storage.getPlaylistName(selected_playlist),
                            new _PlaylistNameCallback()
        {
            @Override public void set(String new_name_)
            {
                int pl_id = SP.Storage.copyPlaylist(selected_playlist, new_name_);
                if(pl_id >= 0)
                    startActivity(new Intent(ListOfPlaylistsActivity.this, PlaylistActivity.class)
                                  .putExtra("playlist-id", pl_id));
                else
                    Toast.makeText(ListOfPlaylistsActivity.this,
                                   R.string.pl_save_error, Toast.LENGTH_SHORT).show();
                
                selected_playlist = -1;
            }
        });
    }
    
    public void onClickMerge()
    {
        new AlertDialog.Builder(this)
            .setCancelable(true).setTitle(R.string.playlist_merge)
            .setItems(SP.Storage.getPlaylistsNames(), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog_, int which_)
            {
                if(SP.Storage.mergePlaylists(selected_playlist, which_))
                    startActivity(new Intent(ListOfPlaylistsActivity.this, PlaylistActivity.class)
                                  .putExtra("playlist-id", selected_playlist));
                else
                    Toast.makeText(ListOfPlaylistsActivity.this,
                                   R.string.pl_save_error, Toast.LENGTH_SHORT).show();
                
                selected_playlist = -1;
            }
        }).show();
    }
    
    public void onClickRemove()
    {
        new AlertDialog.Builder(this)
            .setCancelable(true).setMessage(getString(R.string.playlist_remove_message,
                                                      SP.Storage.getPlaylistName(selected_playlist)))
            .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                SP.Storage.removePlaylist(selected_playlist);
                lofpl_adapter.notifyDataSetChanged();
                
                selected_playlist = -1;
            }
        }).show();
    }
}
