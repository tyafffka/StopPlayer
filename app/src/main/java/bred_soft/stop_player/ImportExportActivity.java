package bred_soft.stop_player;

import java.io.File;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import bred_soft.stop_player.player.StopPlayer;
import edu.android.openfiledialog.OpenFileDialog;

import static bred_soft.utils.AutoBindService.getService;



@SuppressWarnings("WeakerAccess")
public class ImportExportActivity extends Activity
    implements ServiceConnection
{
    public EditText edit_export;
    
    protected StopPlayer SP = null;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.import_export);
        
        edit_export = (EditText)findViewById(R.id.edit_export);
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        bindService(new Intent(this, StopPlayer.class), this, BIND_AUTO_CREATE);
    }
    
    @Override
    public void onPause()
    {
        onServiceDisconnected(null); unbindService(this);
        super.onPause();
    }
    
    @Override
    public void onServiceConnected(ComponentName name, IBinder binder_)
    {   SP = getService(binder_);   }
    
    @Override
    public void onServiceDisconnected(ComponentName name)
    {   SP = null;   }
    
    
    // -*- * -*-
    
    public void on_button_select(View v)
    {
        new OpenFileDialog(this)
            .setDirectoryMode()
            .setOpenDialogListener(new OpenFileDialog.OpenDialogListener()
        {
            @Override public void OnSelectedFile(String dir_name_)
            {
                if(dir_name_ == null || dir_name_.isEmpty()) return;
                edit_export.setText(dir_name_);
            }
        }).show();
    }
    
    public void on_button_export(View v)
    {
        if(SP == null) return;
        
        File dir = new File(edit_export.getText().toString());
        if(! dir.isDirectory())
        {
            Toast.makeText(this, R.string.export_look_error, Toast.LENGTH_LONG).show();
            return;
        }
        
        if(SP.Storage.exportFiles(dir))
            Toast.makeText(this, getString(R.string.export_success, dir.getAbsolutePath()),
                           Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, R.string.export_error, Toast.LENGTH_LONG).show();
    }
    
    public void on_button_import(View v)
    {
        if(SP == null) return;
        
        File dir = new File(edit_export.getText().toString());
        if(! dir.isDirectory())
        {
            Toast.makeText(this, R.string.export_look_error, Toast.LENGTH_LONG).show();
            return;
        }
        
        if(SP.Storage.importFiles(dir))
            Toast.makeText(this, getString(R.string.import_success, dir.getAbsolutePath()),
                           Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, R.string.import_error, Toast.LENGTH_LONG).show();
    }
}
