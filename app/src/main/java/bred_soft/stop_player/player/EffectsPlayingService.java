package bred_soft.stop_player.player;

import java.util.TreeMap;

import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;



@SuppressWarnings("WeakerAccess")
class EffectsPlayingService extends AutoMutedPlayingService
{
    private TreeMap<String, Short> __system_presets = new TreeMap<>();
    
    protected Equalizer _equalizer;
    protected BassBoost _booster;
    
    
    @Override
    public void onCreate()
    {
        super.onCreate();
        
        _equalizer = new Equalizer(0, _player.getAudioSessionId());
        _booster = new BassBoost(0, _player.getAudioSessionId());
        
        __load_effects();
        
        _equalizer.setEnabled(true);
        _booster.setEnabled(true);
    }
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        
        _equalizer.release(); _equalizer = null;
        _booster.release(); _booster = null;
        
        __system_presets = null;
    }
    
    
    // -*- * -*-
    
    /* Устанавливает текущий коэффициент усиления (`level`, дБ)
     * указанной полосы эквалайзера (`band`). */
    public void setEqualizerLevel(short band, int level)
    {
        _equalizer.setBandLevel(band, (short)level);
        Storage.currentEqu[band] = level;
    }
    
    /* Возвращает массив текущих значений
     * коэффициентов усиления (дБ) всех полос эквалайзера. */
    public int[] getEqualizerLevels()
    {
        short n = (short)Storage.currentEqu.length;
        int[] r = new int[n];
        
        for(short k = 0; k < n; ++k) r[k] = _equalizer.getBandLevel(k);
        return r;
    }
    
    /* Устанавливает коэффициенты усиления для эквалайзера
     * из предустановки с указанным именем, если таковая имеется.
     * Сохраняет настройки эквалайзера. */
    public void useEqualizerPreset(String name_)
    {
        short n = (short)Storage.currentEqu.length;
        
        Short system_preset = __system_presets.get(name_);
        if(system_preset != null)
        {
            _equalizer.usePreset(system_preset);
            for(short k = 0; k < n; ++k)
                Storage.currentEqu[k] = _equalizer.getBandLevel(k);
        }
        else
        {
            int[] user_preset = Storage.userEquPresets.get(name_);
            if(user_preset == null) return;
            
            if(user_preset.length < n) n = (short)user_preset.length;
            for(short k = 0; k < n; ++k)
            {
                _equalizer.setBandLevel(k, (short)user_preset[k]);
                Storage.currentEqu[k] = user_preset[k];
            }
        }
        Storage.saveEffects();
    }
    
    /* Сохраняет текущие коэффициенты усиления эквалайзера как
     * предустановку с указанным именем. Сохраняет настройки эквалайзера. */
    public void saveEqualizerPreset(String name_)
    {
        if(name_.isEmpty()) return;
        if(isSystemEqualizerPreset(name_)) return;
        
        short n = _equalizer.getNumberOfBands();
        int[] preset = new int[n];
        
        for(short k = 0; k < n; ++k) preset[k] = _equalizer.getBandLevel(k);
        
        Storage.userEquPresets.put(name_, preset);
        Storage.saveEffects();
    }
    
    /* Удаляет указанную предустановку эквалайзера,
     * если она была ранее сохранена. */
    public void removeEqualizerPreset(String name_)
    {
        if(Storage.userEquPresets.remove(name_) != null)
            Storage.saveEffects();
    }
    
    // ---
    
    /* Устанавливает коэффициент усиления басов. */
    public void setBassLevel(int level)
    {
        _booster.setStrength((short)level);
        Storage.currentBassLevel = level;
    }
    
    /* Возвращает коэффициент усиления басов. */
    public int getBassLevel()
    {   return (int)_booster.getRoundedStrength();   }
    
    
    // -*- * -*-
    
    /* Возвращает массив из двух значений: минимума и максимума
     * коэффициентов усиления эквалайзера. */
    public int[] getEqualizerRange()
    {
        short[] range = _equalizer.getBandLevelRange();
        return new int[]{range[0], range[1]};
    }
    
    /* Возвращает массив частот полос эквалайзера. */
    public int[] getEqualizerFrequencies()
    {
        short n = _equalizer.getNumberOfBands();
        int[] r = new int[n];
        
        for(short k = 0; k < n; ++k) r[k] = _equalizer.getCenterFreq(k);
        return r;
    }
    
    /* Возвращает массив имён всех предустановок,
     * включая те, что предоставляет система. */
    public String[] getEqualizerPresets()
    {
        String[] r = new String[__system_presets.size() + Storage.userEquPresets.size()];
        short k = 0;
        
        for(String key : __system_presets.keySet()) r[k++] = key;
        for(String key : Storage.userEquPresets.keySet()) r[k++] = key;
        return r;
    }
    
    /* Возвращает, является ли указанная предустановка системной.
     * В этом случае её нельзя удалить или перезаписать. */
    public boolean isSystemEqualizerPreset(String name_)
    {   return(__system_presets.get(name_) != null);   }
    
    
    // -*- * -*-
    
    private void __load_effects()
    {
        short n = _equalizer.getNumberOfBands(), n_crop = n;
        
        if(Storage.currentEqu == null)
        {
            Storage.currentEqu = new int[n]; n_crop = 0;
        }
        else if(Storage.currentEqu.length != n)
        {
            int[] equ = Storage.currentEqu;
            Storage.currentEqu = new int[n];
            
            if(equ.length < n_crop) n_crop = (short)equ.length;
            System.arraycopy(equ, 0, Storage.currentEqu, 0, n_crop);
        }
        
        for(short k = 0; k < n_crop; ++k)
            _equalizer.setBandLevel(k, (short)Storage.currentEqu[k]);
        for(short k = n_crop; k < n; ++k)
            Storage.currentEqu[k] = _equalizer.getBandLevel(k);
        
        _booster.setStrength((short)Storage.currentBassLevel);
        
        short count = _equalizer.getNumberOfPresets();
        for(short k = 0; k < count; ++k)
            __system_presets.put(_equalizer.getPresetName(k), k);
    }
}
