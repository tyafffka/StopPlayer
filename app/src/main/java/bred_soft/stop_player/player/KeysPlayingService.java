package bred_soft.stop_player.player;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaMetadataRetriever;
import android.media.RemoteControlClient;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.widget.Toast;

import bred_soft.stop_player.RemoteControlReceiver;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.stop_player.storage.MusicStorage.Playlist;



@SuppressWarnings("WeakerAccess")
class KeysPlayingService extends EffectsPlayingService
{
    public static final long HEADSETHOOK_CLICKS_DELAY = 600 /*ms*/;
    
    protected Handler _headsethook_handler = new Handler();
    protected int _headsethook_clicks_count = 0;
    
    protected RemoteControlClient _control_client = null;
    protected ComponentName _receiver_component = null;
    protected MediaSession _media_session = null;
    
    private PlaybackState.Builder __cached_state = null;
    
    
    @Override
    public void onCreate()
    {
        Context context = getApplicationContext();
        
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            _media_session = new MediaSession(context, "StopPlayerMediaSession");
        }
        
        super.onCreate();
        
        _receiver_component = new ComponentName(
            getPackageName(), RemoteControlReceiver.class.getCanonicalName()
        );
        PendingIntent intent = PendingIntent.getBroadcast(
            context, 0,
            new Intent(Intent.ACTION_MEDIA_BUTTON).setComponent(_receiver_component),
            0
        );
        
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            _media_session.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS |
                                    MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
            _media_session.setCallback(new MediaSession.Callback()
            {
                @Override public boolean onMediaButtonEvent(Intent intent_)
                {
                    if(Intent.ACTION_MEDIA_BUTTON.equals(intent_.getAction()))
                        handleMediaButton(intent_);
                    return true;
                }
            });
            __cached_state = new PlaybackState.Builder()
                .setActions(PlaybackState.ACTION_PLAY_PAUSE | PlaybackState.ACTION_PLAY |
                            PlaybackState.ACTION_PAUSE | PlaybackState.ACTION_STOP |
                            PlaybackState.ACTION_SKIP_TO_NEXT | PlaybackState.ACTION_SKIP_TO_PREVIOUS)
                .setState(PlaybackState.STATE_STOPPED, PlaybackState.PLAYBACK_POSITION_UNKNOWN,
                          1.0f, SystemClock.elapsedRealtime());
            
            _media_session.setPlaybackState(__cached_state.build());
            _media_session.setActive(true);
        }
        else
        {
            ((AudioManager)getSystemService(AUDIO_SERVICE))
                .registerMediaButtonEventReceiver(_receiver_component);
            
            _control_client = new RemoteControlClient(intent);
            _control_client.setTransportControlFlags(
                RemoteControlClient.FLAG_KEY_MEDIA_PLAY_PAUSE |
                RemoteControlClient.FLAG_KEY_MEDIA_PLAY       |
                RemoteControlClient.FLAG_KEY_MEDIA_PAUSE      |
                RemoteControlClient.FLAG_KEY_MEDIA_STOP       |
                RemoteControlClient.FLAG_KEY_MEDIA_NEXT       |
                RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS
            );
            ((AudioManager)getSystemService(AUDIO_SERVICE))
                .registerRemoteControlClient(_control_client);
        }
        
        addFeedback(new _RemoteControlFeedback(), true);
    }
    
    @Override
    public void onDestroy()
    {
        if(_control_client != null)
        {
            ((AudioManager)getSystemService(AUDIO_SERVICE))
                .unregisterRemoteControlClient(_control_client);
            _control_client = null;
            
            ((AudioManager)getSystemService(AUDIO_SERVICE))
                .unregisterMediaButtonEventReceiver(_receiver_component);
            _receiver_component = null;
        }
        
        super.onDestroy();
        
        if(_media_session != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            _media_session.release(); _media_session = null;
            __cached_state = null;
        }
    }
    
    
    // -*- * -*-
    
    protected Runnable clikcsDetect = new Runnable()
    {
        @Override public void run()
        {
            switch(_headsethook_clicks_count)
            {
            case 1: PlayPause(); break;
            case 2: Stop();      break;
            default:
                Toast.makeText(KeysPlayingService.this, "Хорош долбить по кнопке!",
                               Toast.LENGTH_LONG).show(); // (-_1_-)
            }
            _headsethook_clicks_count = 0;
        }
    };
    
    public boolean handleMediaButton(Intent intent_)
    {
        KeyEvent key = intent_.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
        if(key == null) return false;
        if(key.getAction() != KeyEvent.ACTION_DOWN) return false;
        
        switch(key.getKeyCode())
        {
        case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE: PlayPause(); return true;
        case KeyEvent.KEYCODE_MEDIA_PAUSE:      Pause();     return true;
        case KeyEvent.KEYCODE_MEDIA_PLAY:       Play();      return true;
        case KeyEvent.KEYCODE_MEDIA_STOP:       Stop();      return true;
        case KeyEvent.KEYCODE_MEDIA_NEXT:       Next();      return true;
        case KeyEvent.KEYCODE_MEDIA_PREVIOUS:   Previous();  return true;
        case KeyEvent.KEYCODE_HEADSETHOOK:
            if(++_headsethook_clicks_count == 1)
                _headsethook_handler.postDelayed(clikcsDetect, HEADSETHOOK_CLICKS_DELAY);
            return true;
        default:
            return false;
        }
    }
    
    
    // -*- Self feedback class -*-
    
    public class _RemoteControlFeedback extends PlayingFeedbackItem
    {
        @Override
        public void onSetPlaylist(Playlist playlist_) {}
        
        @Override
        public void onSetTrack(int playlist_i_, Item info_)
        {
            if(_control_client != null)
            {
                RemoteControlClient.MetadataEditor ed = _control_client.editMetadata(true);
                
                ed.putString(MediaMetadataRetriever.METADATA_KEY_ARTIST, info_.artist);
                ed.putString(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST, info_.artist); // Лично у меня KEY_ARTIST не срабатывает, а это - да.
                ed.putString(MediaMetadataRetriever.METADATA_KEY_TITLE, info_.trackname);
                ed.putLong(MediaMetadataRetriever.METADATA_KEY_DURATION, info_.duration);
                ed.apply();
            }
            if(_media_session != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                MediaMetadata meta = new MediaMetadata.Builder()
                    .putString(MediaMetadata.METADATA_KEY_ARTIST, info_.artist)
                    .putString(MediaMetadata.METADATA_KEY_ALBUM_ARTIST, info_.artist)
                    .putString(MediaMetadata.METADATA_KEY_TITLE, info_.trackname)
                    .putLong(MediaMetadata.METADATA_KEY_DURATION, info_.duration)
                    .build();
                _media_session.setMetadata(meta);
            }
        }
        
        @Override
        public void onSetDuration(int duration_) {}
        
        @Override
        public void onStop()
        {
            if(_control_client != null)
                _control_client.setPlaybackState(RemoteControlClient.PLAYSTATE_STOPPED);
            
            if(_media_session != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                __cached_state.setState(PlaybackState.STATE_STOPPED, PlaybackState.PLAYBACK_POSITION_UNKNOWN,
                                        1.0f, SystemClock.elapsedRealtime());
                _media_session.setPlaybackState(__cached_state.build());
            }
        }
        
        @Override
        public void onPlay()
        {
            if(_control_client != null)
                _control_client.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
            
            if(_media_session != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                __cached_state.setState(PlaybackState.STATE_PLAYING, PlaybackState.PLAYBACK_POSITION_UNKNOWN,
                                        1.0f, SystemClock.elapsedRealtime());
                _media_session.setPlaybackState(__cached_state.build());
            }
        }
        
        @Override
        public void onPause()
        {
            if(_control_client != null)
                _control_client.setPlaybackState(RemoteControlClient.PLAYSTATE_PAUSED);
            
            if(_media_session != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                __cached_state.setState(PlaybackState.STATE_PAUSED, PlaybackState.PLAYBACK_POSITION_UNKNOWN,
                                        1.0f, SystemClock.elapsedRealtime());
                _media_session.setPlaybackState(__cached_state.build());
            }
        }
        
        @Override
        public void onTrackTimeUpdate(int time_) {}
        
        @Override
        public void onSpectorUpdate(float[] spector_) {}
    }
}
