package bred_soft.stop_player.player;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.stop_player.storage.MusicStorage.Playlist;



@SuppressWarnings("WeakerAccess")
public abstract class PlayingFeedbackItem
{
    /* Вызывается, когда плееру установлен новый плейлист. */
    public abstract void onSetPlaylist(Playlist playlist_);
    /* Вызывается, когда изменяется текущий трек. */
    public abstract void onSetTrack(int playlist_i_, Item info_);
    /* Вызывается при получении длительности трека из плеера (в миллисекундах). */
    public abstract void onSetDuration(int duration_);
    /* Вызывается, когда воспроизведение останавливается. */
    public abstract void onStop();
    /* Вызывается, когда воспроизведение запускается. */
    public abstract void onPlay();
    /* Вызывается, когда воспроизведение приостанавливается. */
    public abstract void onPause();
    /* Вызывается периодически с текущей позицией воспроизведения (в миллисекундах). */
    public abstract void onTrackTimeUpdate(int time_);
    /* Вызывается периодически с данными спектрограммы, если она была включена. */
    public abstract void onSpectorUpdate(float[] spector_);
    
    private int[] _spec_p = null; // if null - don't handle spector
    
    public static int CAPTURE_SIZE = -1;
    public static final double SPECTOR_NORMALIZATION = -4.215 /*dB*/;
    
    
    public void destroy()
    {   _spec_p = null;   }
    
    public void _spector(float[] magnitudes_, int ma_length_)
    {
        if(_spec_p == null) return;
        
        float[] spector = new float[_spec_p.length];
        int si;
        
        spector[0] = magnitudes_[0];
        for(si = 1; si < spector.length - 1; ++si) spector[si] = 0.0f;
        spector[si] = magnitudes_[ma_length_];
        
        si = 1;
        for(int mi = 1; mi < ma_length_; ++mi)
        {
            if(mi >= _spec_p[si]) ++si;
            if(magnitudes_[mi] > spector[si]) spector[si] = magnitudes_[mi];
        }
        
        for(si = 0; si < spector.length; ++si)
            spector[si] = (float)(Math.log10(spector[si] + 1.0) + SPECTOR_NORMALIZATION); // добавляем 1, чтобы не получить log(0)
        
        onSpectorUpdate(spector);
    }
    
    /* Включает спектрограмму с указанным количеством полос. Данные
     * спектрограммы агрегируются и приводятся к логарифмическому масштабу.
     * Возвращает, удалось ли установить спектрограмму такого размера. */
    public boolean setSpector(int spector_size_)
    {
        if(spector_size_ <= 0 || CAPTURE_SIZE < spector_size_)
        {
            _spec_p = null; return false;
        }
        _spec_p = new int[spector_size_];
        
        double tau = (double)(spector_size_ - 1);
        for(int i = 0; i < spector_size_; ++i)
        {
            _spec_p[i] = (int)Math.pow(CAPTURE_SIZE, i / tau) + 1;
            if(_spec_p[i] <= i) _spec_p[i] = i + 1;
        }
        return true;
    }
    
    @Override
    public boolean equals(Object o)
    {   return((o instanceof PlayingFeedbackItem) && (this == o));   }
}
