package bred_soft.stop_player.player;

import android.content.Intent;

import bred_soft.stop_player.PlayingWidgetProvider;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.stop_player.storage.MusicStorage.Playlist;



class WidgetPlayingService extends KeysPlayingService
{
    private boolean __is_playing = false;
    private Item __last_info = null;
    
    
    @Override
    public void onCreate()
    {
        super.onCreate();
        addFeedback(new _WidgetFeedback(), true);
    }
    
    
    public class _WidgetFeedback extends PlayingFeedbackItem
    {
        @Override
        public void onSetPlaylist(Playlist playlist_) {}
        
        @Override
        public void onSetTrack(int playlist_i_, Item info_)
        {   sendWidgetInfo(info_);   }
        
        @Override
        public void onSetDuration(int duration_) {}
        
        @Override
        public void onStop()
        {
            __is_playing = false; sendWidgetInfo(null);
        }
        
        @Override
        public void onPlay()
        {
            __is_playing = true; sendWidgetInfo(null);
        }
        
        @Override
        public void onPause()
        {
            __is_playing = false; sendWidgetInfo(null);
        }
        
        @Override
        public void onTrackTimeUpdate(int time) {}
        
        @Override
        public void onSpectorUpdate(float[] spector_) {}
    }
    
    
    public void sendWidgetInfo(Item info_)
    {
        if(info_ == null)
        {
            if(__last_info == null) return;
            info_ = __last_info;
        }
        else
        {   __last_info = info_;   }
        
        sendBroadcast(
            new Intent(this, PlayingWidgetProvider.class)
            .setAction(PlayingWidgetProvider.ACTION_SET_INFO)
            .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
            .putExtra("artist", info_.artist)
            .putExtra("trackname",  info_.trackname)
            .putExtra("is_playing", __is_playing)
        );
    }
}
