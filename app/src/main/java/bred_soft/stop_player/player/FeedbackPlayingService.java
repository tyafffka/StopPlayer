package bred_soft.stop_player.player;

import java.util.HashSet;

import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.media.audiofx.Visualizer.OnDataCaptureListener;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.stop_player.storage.MusicStorage.Playlist;



@SuppressWarnings("WeakerAccess")
class FeedbackPlayingService extends BasePlayingService
    implements OnDataCaptureListener
{
    private Playlist __sended_playlist = null;
    private PlayerState __sended_state = PlayerState.STOPPED;
    private int __sended_playlist_i = -1;
    private Item __sended_info = null;
    private int __sended_duration = -1;
    
    protected HashSet<PlayingFeedbackItem> _feedbacks; // NOTE: здесь должен быть HashSet, потому что в него можно пихать любые объекты. Для TreeSet нужен Comparable
    protected Visualizer _visualizer;
    
    
    // -*- Service methods -*-
    
    @Override
    public void onCreate()
    {
        super.onCreate();
        _feedbacks = new HashSet<>();
        
        _visualizer = new Visualizer(_player.getAudioSessionId());
        _visualizer.setEnabled(false);
        
        PlayingFeedbackItem.CAPTURE_SIZE = Visualizer.getCaptureSizeRange()[1] >> 1;
        
        _visualizer.setDataCaptureListener(this, Visualizer.getMaxCaptureRate(), false, true);
        _visualizer.setCaptureSize(PlayingFeedbackItem.CAPTURE_SIZE << 1);
        
        __sended_playlist = Storage.currentPlaylist;
        __sended_state = _state;
        if(__sended_playlist != null && __sended_playlist.size() > 0)
        {
            __sended_playlist_i = __sended_playlist.getPosition();
            __sended_info = __sended_playlist.getCurrentItem();
        }
    }
    
    @Override
    public void onDestroy()
    {
        _visualizer.setDataCaptureListener(null, 0, false, false);
        _visualizer.release(); _visualizer = null;
        
        if(_state != PlayerState.STOPPED)
        {
            for(PlayingFeedbackItem f : _feedbacks) f.onStop();
        }
        for(PlayingFeedbackItem f : _feedbacks) f.destroy();
        
        _feedbacks.clear(); _feedbacks = null;
        super.onDestroy();
    }
    
    
    // -*- Using _callbacks -*-
    
    @Override
    public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int i) { /* HOLDED */ }
    
    @Override
    public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int i)
    {
        if(_state != PlayerState.PLAYING)
        {
            visualizer.setEnabled(false); return;
        }
        
        // собственно - спектр
        int ma_length = (bytes.length >> 1) + 1;
        float[] magnitudes = new float[ma_length];
        
        magnitudes[0] = magnitudeQ(bytes[0], 0);
        for(int bi = 2, mi = 1; bi < bytes.length; bi += 2, ++mi)
        {
            magnitudes[mi] = magnitudeQ(bytes[bi], bytes[bi + 1]);
        }
        ma_length -= 1;
        magnitudes[ma_length] = magnitudeQ(bytes[1], 0);
        
        // заодно и время трека
        int pos = _player.getCurrentPosition();
        for(PlayingFeedbackItem f : _feedbacks)
        {
            f.onTrackTimeUpdate(pos);
            f._spector(magnitudes, ma_length);
        }
    }
    
    @Override
    public void onPrepared(MediaPlayer mp)
    {
        super.onPrepared(mp);
        _send_feedback();
        if(_state == PlayerState.PLAYING) _visualizer.setEnabled(true);
    }
    
    @Override
    public void onCompletion(MediaPlayer mp)
    {
        super.onCompletion(mp);
        _send_feedback();
    }
    
    @Override
    public boolean onError(MediaPlayer mp, int i, int i2)
    {
        boolean r = super.onError(mp, i, i2);
        _send_feedback();
        return r;
    }
    
    
    // -*- Overriden methods -*-
    
    @Override
    public boolean setPlaylist(Playlist pl_)
    {
        boolean r = super.setPlaylist(pl_);
        _send_feedback();
        return r;
    }
    
    @Override
    public void Play()
    {
        super.Play(); _send_feedback();
        if(_is_prepared) _visualizer.setEnabled(true);
    }
    
    @Override
    public void Pause()
    {
        super.Pause(); _send_feedback();
    }
    
    @Override
    public void Stop()
    {
        super.Stop(); _send_feedback();
    }
    
    @Override
    public void Next()
    {
        super.Next(); _send_feedback();
    }
    
    @Override
    public void Previous()
    {
        super.Previous(); _send_feedback();
    }
    
    @Override
    public int setTime(int time_)
    {
        time_ = super.setTime(time_);
        for(PlayingFeedbackItem f : _feedbacks) f.onTrackTimeUpdate(time_);
        return time_;
    }
    
    
    // -*- Self methods -*-
    
    /* Добавляет подписчика обратной связи, который получает обновления
     * состояния плеера, включая текущий плейлист, трек, метаданные и
     * позицию воспроизведения. `is_update_` - следует ли сразу же
     * отправить полное сотояние плеера подписчику. */
    public void addFeedback(PlayingFeedbackItem pf_, boolean is_update_)
    {
        _feedbacks.add(pf_);
        if(! is_update_) return;
        
        // update feedback
        
        int i = -1;
        Item info = new Item();
        
        if(Storage.currentPlaylist != null && Storage.currentPlaylist.size() > 0)
        {
            pf_.onSetPlaylist(Storage.currentPlaylist);
            
            i = Storage.currentPlaylist.getPosition();
            info = Storage.currentPlaylist.getCurrentItem();
        }
        
        pf_.onSetTrack(i, info);
        if((_state == PlayerState.PLAYING || _state == PlayerState.PAUSED) &&
           _is_prepared)
        {   pf_.onSetDuration(_player.getDuration());   }
        
        switch(_state)
        {
        case STOPPED: pf_.onStop(); break;
        case PLAYING: pf_.onPlay(); break;
        case PAUSED: pf_.onPause();
        }
        
        pf_.onTrackTimeUpdate(getTime());
    }
    
    /* Удаляет указанного подписчика обратной связи из плеера. */
    public void removeFeedback(PlayingFeedbackItem _pf)
    {   _feedbacks.remove(_pf);   }
    
    
    // -*- internal used -*-
    
    protected void _send_feedback()
    {
        if(__sended_playlist != Storage.currentPlaylist)
        {
            __sended_playlist = Storage.currentPlaylist;
            for(PlayingFeedbackItem f : _feedbacks) f.onSetPlaylist(__sended_playlist);
        }
        
        int i = (__sended_playlist == null || __sended_playlist.size() <= 0)?
                -1 : __sended_playlist.getPosition();
        Item info = (__sended_playlist == null)? null : __sended_playlist.getCurrentItem();
        
        if(__sended_playlist_i != i || __sended_info != info)
        {
            __sended_playlist_i = i; __sended_info = info;
            
            if(info == null) info = new Item();
            else __sended_duration = info.duration;
            
            for(PlayingFeedbackItem f : _feedbacks) f.onSetTrack(i, info);
        }
        
        if((_state == PlayerState.PLAYING || _state == PlayerState.PAUSED) &&
           _is_prepared)
        {
            int duration = _player.getDuration();
            if(duration != __sended_duration)
            {
                __sended_duration = duration;
                for(PlayingFeedbackItem f : _feedbacks) f.onSetDuration(duration);
            }
        }
        
        if(__sended_state != _state)
        {
            switch(__sended_state = _state)
            {
            case STOPPED:
                for(PlayingFeedbackItem f : _feedbacks) f.onStop();
                break;
            case PLAYING:
                for(PlayingFeedbackItem f : _feedbacks) f.onPlay();
                break;
            case PAUSED:
                for(PlayingFeedbackItem f : _feedbacks) f.onPause();
            }
        }
    }
    
    protected static float magnitudeQ(int Re, int Im)
    {   return (float)(Re * Re + Im * Im);   }
}
