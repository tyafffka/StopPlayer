package bred_soft.stop_player.player;

import android.media.AudioManager;

import static android.media.AudioManager.OnAudioFocusChangeListener;



@SuppressWarnings("WeakerAccess")
class AutoMutedPlayingService extends FeedbackPlayingService
    implements OnAudioFocusChangeListener
{
    protected boolean is_muted_playback = false;
    
    
    @Override
    public void onCreate()
    {
        super.onCreate();
        ((AudioManager)getSystemService(AUDIO_SERVICE))
            .requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }
    
    @Override
    public void onDestroy()
    {
        ((AudioManager)getSystemService(AUDIO_SERVICE)).abandonAudioFocus(this);
        super.onDestroy();
    }
    
    
    @Override
    public void onAudioFocusChange(int focusChange)
    {
        switch(focusChange)
        {
        case AudioManager.AUDIOFOCUS_LOSS:
        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
            if(_state == PlayerState.PLAYING)
            {
                is_muted_playback = true; Pause();
            }
            return;
            
        case AudioManager.AUDIOFOCUS_GAIN:
            if(is_muted_playback)
            {
                is_muted_playback = false; Play();
            }
        }
    }
}
