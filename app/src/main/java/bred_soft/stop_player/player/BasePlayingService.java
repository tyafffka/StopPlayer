package bred_soft.stop_player.player;

import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.widget.Toast;

import bred_soft.stop_player.R;
import bred_soft.stop_player.storage.MusicStorage;
import bred_soft.utils.AutoBindService;

import static android.media.MediaPlayer.OnCompletionListener;
import static android.media.MediaPlayer.OnErrorListener;
import static android.media.MediaPlayer.OnPreparedListener;

import static bred_soft.stop_player.storage.MusicStorage.PlayingMode;
import static bred_soft.stop_player.storage.MusicStorage.Playlist;



@SuppressWarnings("WeakerAccess")
public class BasePlayingService extends AutoBindService
    implements OnCompletionListener, OnErrorListener, OnPreparedListener
{
    public enum PlayerState
    {   STOPPED, PLAYING, PAUSED   }
    
    protected MediaPlayer _player = null;
    protected boolean _is_prepared = false;
    protected PlayerState _state = PlayerState.STOPPED;
    
    /* Модель хранилища, используемая плеером. */
    public MusicStorage Storage = null;
    
    
    // -*- Service methods -*-
    
    @Override
    public void onCreate()
    {
        super.onCreate();
        
        _player = new MediaPlayer();
        _player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        _player.setOnCompletionListener(this);
        _player.setOnErrorListener(this);
        _player.setOnPreparedListener(this);
        
        Storage = new MusicStorage(this);
        if(Storage.currentPlaylist != null)
            Storage.currentPlaylist.setPosition(Storage.currentPosition);
    }
    
    @Override
    public void onDestroy()
    {
        if(_state != PlayerState.STOPPED) _player.stop();
        
        _player.setOnCompletionListener(null);
        _player.setOnErrorListener(null);
        _player.setOnPreparedListener(null);
        _player.release(); _player = null;
        
        Storage.destroy(); Storage = null;
        super.onDestroy();
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {   return Service.START_STICKY;   }
    
    @Override
    public void onTaskRemoved(Intent intent)
    {   stopSelf();   }
    
    
    // -*- Handlers -*-
    
    @Override
    public void onPrepared(MediaPlayer mp)
    {
        _is_prepared = true;
        if(_state == PlayerState.PLAYING) mp.start();
    }
    
    @Override
    public void onCompletion(MediaPlayer mp)
    {
        if(Storage.currentMode == PlayingMode.LOOP_ONE) // short way
        {   mp.start(); return;   }
        
        if(Storage.currentPlaylist == null)
        {   Stop(); return;   }
        
        if(Storage.currentPlaylist.toNext(Storage.currentMode, false /*automatic*/))
        {   _playback();   }
        else
        {   Stop();   }
        
        Storage.currentPosition = Storage.currentPlaylist.getPosition();
        Storage.savePlayback();
    }
    
    @Override
    public boolean onError(MediaPlayer mp, int i, int i2)
    {
        _state = PlayerState.STOPPED; mp.reset();
        
        Toast.makeText(this, R.string.some_error, Toast.LENGTH_SHORT).show();
        return false;
    }
    
    
    // -*- Player methods -*-
    
    /* Останавливает воспроизведение, если оно было, и принимает указанный
     * плейлист в качестве текущего. Выбор текущего трека не производится.
     * Возвращает, валидный ли плейлист был передан. */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean setPlaylist(Playlist pl_)
    {
        if(pl_ == null || pl_.size() <= 0) return false;
        
        Stop(); Storage.currentPlaylist = pl_;
        return true;
    }
    
    /* Выбирает трек с указанным номером в качестве текущего и запускает его
     * воспроизведение. Возвращает, удалось ли выбрать трек. */
    public boolean start(int playlist_i_)
    {
        if(Storage.currentPlaylist == null) return false;
        if(! Storage.currentPlaylist.setPosition(playlist_i_)) return false;
        
        _state = PlayerState.PLAYING; _playback();
        return true;
    }
    
    /* Принимает указанный плейлист в качестве текущего и запускает
     * его воспризведение. Если режим воспроизведения - перемешанный,
     * воспроизведение начнётся со случайного трека.
     * Возвращает, валидный ли плейлист был передан. */
    public boolean start(Playlist pl_)
    {
        if(! setPlaylist(pl_)) return false;
        Storage.currentPlaylist.toNext(Storage.currentMode);
        
        _state = PlayerState.PLAYING; _playback();
        return true;
    }
    
    /* Принимает указанный плейлист в качестве текущего и запускает
     * его воспризведение с указанного трека. Возвращает,
     * валидный ли плейлист был передан и удалось ли выбрать трек. */
    public boolean start(Playlist pl_, int playlist_i_)
    {
        if(! setPlaylist(pl_)) return false;
        if(! Storage.currentPlaylist.setPosition(playlist_i_)) return false;
        
        _state = PlayerState.PLAYING; _playback();
        return true;
    }
    
    /* Запускает воспроизведение текущего трека из состояния "играет".
     * В случае ошибок переходит в состояние "остановлен". */
    protected void _playback()
    {
        _player.reset(); _is_prepared = false;
        
        if((_state != PlayerState.PLAYING && _state != PlayerState.PAUSED) ||
           Storage.currentPlaylist == null || Storage.currentPlaylist.size() <= 0)
        {
            _state = PlayerState.STOPPED; return;
        }
        
        Storage.currentPosition = Storage.currentPlaylist.getPosition();
        Storage.savePlayback();
        try
        {
            _player.setDataSource(Storage.currentPlaylist.getCurrentURI());
            _player.prepareAsync();
        }
        catch(IOException e)
        {
            _state = PlayerState.STOPPED; _player.reset();
            Toast.makeText(this, R.string.load_error, Toast.LENGTH_SHORT).show();
        }
    }
    
    
    // -*- Playback methods -*-
    
    /* Запускает или возобновляет воспроизведение,
     * если плеер не находился в состоянии "играет". */
    public void Play()
    {
        switch(_state)
        {
        case STOPPED:
            _state = PlayerState.PLAYING; _playback();
            return;
            
        case PAUSED:
            _state = PlayerState.PLAYING;
            if(_is_prepared) _player.start();
        }
    }
    
    /* Приостанавливает воспроизведение, если оно было запущено. */
    public void Pause()
    {
        if(_state == PlayerState.STOPPED) return;
        
        _state = PlayerState.PAUSED;
        if(_player.isPlaying()) _player.pause();
    }
    
    /* Запускает или приостанавливает возпроизведение в зависимости от того,
     * находится ил плеер в состоянии "играет". */
    public void PlayPause()
    {
        if(_state == PlayerState.PLAYING) Pause();
        else Play();
    }
    
    /* Останавливает воспроизведение. */
    public void Stop()
    {
        if(_state == PlayerState.STOPPED) return;
        
        _player.stop(); _player.reset();
        _state = PlayerState.STOPPED; _is_prepared = false;
    }
    
    /* Переходит к следующему треку в зависимости от режима воспроизведения.
     * Сохраняет состояние плеера. */
    public void Next()
    {
        if(Storage.currentPlaylist != null &&
           Storage.currentPlaylist.toNext(Storage.currentMode) &&
           _state == PlayerState.PLAYING)
        {
            _player.stop(); _playback();
        }
        else
        {   Stop();   }
    }
    
    /* Переходит к предыдущему треку, сохраняя состояние плеера. */
    public void Previous()
    {
        if(Storage.currentPlaylist != null &&
           Storage.currentPlaylist.toPrevious(Storage.currentMode) &&
           _state == PlayerState.PLAYING)
        {
            _player.stop(); _playback();
        }
        else
        {   Stop();   }
    }
    
    /* Возвращает текущую позицию воспроизведения (в миллисекундах),
     * если плеер находится в состоянии "играет", иначе -1. */
    public int getTime()
    {   return _is_prepared ? _player.getCurrentPosition() : -1;   }
    
    /* Устанавливает позицию воспроизведения (в миллисекундах) в пределах
     * длительности трека, если плеер находится в состоянии "играет".
     * Возвращает установленную позицию. */
    @SuppressWarnings("UnusedReturnValue")
    public int setTime(int time_)
    {
        if(! _is_prepared) return -1;
        
        int duration = _player.getDuration();
        if(time_ >= duration) time_ = duration - 1;
        if(time_ < 0) time_ = 0;
        
        _player.seekTo(time_);
        return time_;
    }
}
