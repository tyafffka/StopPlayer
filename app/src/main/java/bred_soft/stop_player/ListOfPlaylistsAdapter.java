package bred_soft.stop_player;

import java.util.TreeSet;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import bred_soft.stop_player.storage.MusicStorage;
import bred_soft.utils.ListButtonsClicker;

import static bred_soft.utils.ListButtonsClicker.OnListButtonClickListener;



@SuppressWarnings("WeakerAccess")
public class ListOfPlaylistsAdapter extends BaseAdapter
    implements OnListButtonClickListener, SearchView.OnQueryTextListener
{
    interface ClickListener
    {
        void onItemClick(long item_id);
        void onItemDoc(long item_id);
    }
    
    public static final short TAG_ITEM = 0;
    public static final short TAG_ITEM_DOC = 10;
    
    
    protected MusicStorage _storage;
    private TreeSet<Integer> __filtered = new TreeSet<>();
    
    private ListButtonsClicker __clicker;
    protected ClickListener _listener;
    
    protected LayoutInflater _inflater;
    protected ListView _list;
    
    
    public ListOfPlaylistsAdapter(Activity activity_, ClickListener listener_,
                                  ListView list_, MusicStorage storage_)
    {
        _storage = storage_;
        
        __clicker = new ListButtonsClicker(this);
        _listener = listener_;
        
        _inflater = activity_.getLayoutInflater();
        _list = list_; _list.setAdapter(this);
        notifyDataSetChanged();
    }
    
    public void destroy()
    {
        _inflater = null;
        _list.setAdapter(null); _list = null;
        
        __clicker.destroy(); __clicker = null;
        _listener = null;
        
        _storage = null;
        __filtered.clear(); __filtered = null;
    }
    
    @Override
    public void onListButtonClick(int position_, short tag_)
    {
        long id = getItemId(position_);
        switch(tag_)
        {
        case TAG_ITEM: _listener.onItemClick(id); return;
        case TAG_ITEM_DOC: _listener.onItemDoc(id);
        }
    }
    
    @Override
    public boolean onQueryTextChange(String query_)
    {   return onQueryTextSubmit(query_);   }
    
    @Override
    public boolean onQueryTextSubmit(String query_)
    {
        __filtered.clear();
        
        if(! query_.isEmpty())
        {
            boolean is_found = false;
            query_ = query_.toLowerCase();
            
            int count = _storage.playlistsCount();
            for(int i = 0; i < count; ++i)
            {
                String name = _storage.getPlaylistName(i);
                if(! name.toLowerCase().contains(query_)) continue;
                
                if(! is_found)
                {
                    _list.smoothScrollToPosition(i); is_found = true;
                }
                __filtered.add(i);
            }
        }
        
        notifyDataSetChanged();
        return true;
    }
    
    
    @Override
    public int getCount() {   return _storage.playlistsCount();   }
    
    @Override
    public String getItem(int position_)
    {   return _storage.getPlaylistName(position_);   }
    
    @Override
    public long getItemId(int position_)
    {
        return (0 <= position_ && position_ < _storage.playlistsCount())? position_ : -1;
    }
    
    @Override
    public View getView(int position_, View row_, ViewGroup parent_)
    {
        if(row_ == null)
        {
            row_ = _inflater.inflate(R.layout.lofpl_item, parent_, false);
            if(row_ == null) return null;
        }
        
        if(__filtered.contains(position_))
            row_.setBackgroundResource(R.color.search_hili);
        else
            row_.setBackgroundResource(R.color.transparent);
        
        __clicker.setOnClick(row_.findViewById(R.id.lofpl_main), position_, TAG_ITEM);
        __clicker.setOnClick(row_.findViewById(R.id.button_lofpl_dialog), position_, TAG_ITEM_DOC);
        
        ((TextView)row_.findViewById(R.id.lofpl_name)).setText(getItem(position_));
        return row_;
    }
}
