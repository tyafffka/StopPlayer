package bred_soft.stop_player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

import bred_soft.stop_player.player.StopPlayer;

import static bred_soft.utils.AutoBindService.autoPeekService;



public class RemoteControlReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context_, Intent intent_)
    {
        StopPlayer SP = autoPeekService(this, context_, StopPlayer.class);
        if(SP == null) return; // Приложение не запущено.
        
        String action = intent_.getAction();
        
        if(AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(action))
        {
            SP.Pause();
        }
        else if(Intent.ACTION_MEDIA_BUTTON.equals(action))
        {
            SP.handleMediaButton(intent_);
        }
    }
}
