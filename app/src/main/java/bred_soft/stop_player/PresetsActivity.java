package bred_soft.stop_player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import bred_soft.stop_player.player.StopPlayer;
import bred_soft.utils.ListButtonsClicker;

import static bred_soft.utils.AutoBindService.getService;
import static bred_soft.utils.ListButtonsClicker.OnListButtonClickListener;



@SuppressWarnings("WeakerAccess")
public class PresetsActivity extends Activity
    implements ServiceConnection
{
    private class Adapter extends BaseAdapter
        implements OnListButtonClickListener, SearchView.OnQueryTextListener
    {
        public static final short TAG_ITEM = 0;
        public static final short TAG_ITEM_REMOVE = 10;
        
        public String[] names = null;
        public boolean[] found = null;
        
        private ListButtonsClicker __clicker = new ListButtonsClicker(this);
        
        
        public void destroy()
        {
            __clicker.destroy(); __clicker = null;
            names = null; found = null;
        }
        
        @Override
        public void onListButtonClick(int position_, short tag_)
        {
            switch(tag_)
            {
            case TAG_ITEM: onItemSelect(names[position_]); return;
            case TAG_ITEM_REMOVE: onItemRemove(names[position_]);
            }
        }
        
        @Override
        public boolean onQueryTextChange(String query_)
        {   return onQueryTextSubmit(query_);   }
        
        @Override
        public boolean onQueryTextSubmit(String query_)
        {
            if(names == null) return true;
            if(found == null) found = new boolean[names.length];
            
            if(! query_.isEmpty())
            {
                boolean is_found = false;
                query_ = query_.toLowerCase();
                
                for(int i = 0; i < names.length; ++i)
                {
                    if(! (found[i] = names[i].toLowerCase().contains(query_)))
                        continue;
                    
                    if(! is_found)
                    {
                        list_presets.smoothScrollToPosition(i); is_found = true;
                    }
                }
            }
            else
            {
                for(int i = 0; i < found.length; ++i) found[i] = false;
            }
            
            notifyDataSetChanged();
            return true;
        }
        
        
        @Override
        public int getCount()
        {   return (names != null)? names.length : 0;   }
        
        @Override
        public String getItem(int i_)
        {   return (names != null)? names[i_] : null;   }
        
        @Override
        public long getItemId(int i_)
        {   return (names != null && 0 <= i_ && i_ < names.length)? i_ : -1;   }
        
        @Override
        public View getView(int position_, View row_, ViewGroup parent_)
        {
            if(row_ == null)
            {
                row_ = getLayoutInflater().inflate(R.layout.presets_item, parent_, false);
                if(row_ == null) return null;
            }
            
            String item = getItem(position_);
            boolean is_system = SP.isSystemEqualizerPreset(item);
            
            if(found != null && found[position_])
                row_.setBackgroundResource(R.color.search_hili);
            else
                row_.setBackgroundResource(R.color.transparent);
            
            __clicker.setOnClick(row_.findViewById(R.id.presets_main), position_, TAG_ITEM);
            
            View button_remove = row_.findViewById(R.id.button_presets_remove);
            button_remove.setVisibility(is_system ? View.GONE : View.VISIBLE);
            if(! is_system) __clicker.setOnClick(button_remove, position_, TAG_ITEM_REMOVE);
            
            ((TextView)row_.findViewById(R.id.presets_name)).setText(item);
            return row_;
        }
    } //- class Adapter
    
    // --- *** ---
    
    
    public SearchView menu_preset_search = null;
    public ListView list_presets;
    
    protected StopPlayer SP = null;
    protected Adapter presets_adapter = new Adapter();
    
    
    @Override
    public void onCreate(Bundle savedInstanceState_)
    {
        super.onCreate(savedInstanceState_);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.presets);
        
        list_presets = (ListView)findViewById(R.id.list_presets);
        list_presets.setAdapter(presets_adapter);
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        bindService(new Intent(this, StopPlayer.class), this, BIND_AUTO_CREATE);
    }
    
    @Override
    public void onPause()
    {
        onServiceDisconnected(null); unbindService(this);
        super.onPause();
    }
    
    @Override
    protected void onDestroy()
    {
        list_presets.setAdapter(null);
        list_presets.setOnItemClickListener(null);
        list_presets = null;
        
        menu_preset_search.setOnQueryTextListener(null);
        menu_preset_search = null;
        
        presets_adapter.destroy(); presets_adapter = null;
        super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu_)
    {
        getMenuInflater().inflate(R.menu.presets, menu_);
        
        menu_preset_search = (SearchView)menu_.findItem(R.id.menu_preset_search)
                                              .getActionView();
        menu_preset_search.setOnQueryTextListener(presets_adapter);
        
        return super.onCreateOptionsMenu(menu_);
    }
    
    @Override
    public void onServiceConnected(ComponentName name_, IBinder binder_)
    {
        SP = getService(binder_);
        presets_adapter.names = SP.getEqualizerPresets();
        presets_adapter.found = null;
        presets_adapter.notifyDataSetChanged();
    }
    
    @Override
    public void onServiceDisconnected(ComponentName name_)
    {
        presets_adapter.names = null;
        presets_adapter.found = null;
        presets_adapter.notifyDataSetChanged();
        SP = null;
    }
    
    // --- *** ---
    
    
    public void onItemSelect(final String name_)
    {
        if(SP == null) return;
        
        SP.useEqualizerPreset(name_);
        finish();
    }
    
    public void onItemRemove(final String name_)
    {
        if(SP == null) return;
        
        new AlertDialog.Builder(this)
            .setCancelable(true).setMessage(getString(R.string.preset_remove_message, name_))
            .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                SP.removeEqualizerPreset(name_);
                
                presets_adapter.names = SP.getEqualizerPresets();
                presets_adapter.found = null;
                presets_adapter.notifyDataSetChanged();
            }
        }).show();
    }
}
