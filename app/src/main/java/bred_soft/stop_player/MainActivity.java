package bred_soft.stop_player;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.*;
import android.widget.*;

//import com.mobeta.android.dslv.DragSortListView;
import bred_soft.stop_player.player.StopPlayer;

import static android.widget.SeekBar.OnSeekBarChangeListener;
import static android.view.View.OnFocusChangeListener;

import static bred_soft.stop_player.storage.MusicStorage.PlayingMode;
import static bred_soft.stop_player.PlaylistAdapter.ClickListener;
import static bred_soft.utils.AutoBindService.getService;



@SuppressWarnings("WeakerAccess")
public class MainActivity extends Activity
    implements ServiceConnection, ClickListener, OnSeekBarChangeListener,
               OnFocusChangeListener
{
    public static final int TIME_SKIP_DELTA = 5000 /*ms*/;
    
    public SearchView menu_main_track_search = null;
    public View layout_info, layout_seek,
        layout_buttons_play, layout_buttons_mode;
    
    public SurfaceView image_spector;
    public TextView text_artist;
    public TextView text_trackname;
    public TextView text_cur_time;
    public TextView text_full_time;
    public SeekBar  seek_time;
    public ListView list_playlist;
    
    public ImageButton button_playpause;
    public ImageButton button_mode_normal;
    public ImageButton button_mode_loop;
    public ImageButton button_mode_random;
    public ImageButton button_mode_one;
    public ImageButton button_mode_loop_one;
    
    protected MainActivityCallbacks _callbacks;
    protected StopPlayer SP = null;
    public PlaylistAdapter playlist_adapter = null;
    public boolean is_playlist_saved = true;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.main);
        
        layout_info         = findViewById(R.id.layout_info);
        layout_seek         = findViewById(R.id.layout_seek);
        layout_buttons_play = findViewById(R.id.layout_buttons_play);
        layout_buttons_mode = findViewById(R.id.layout_buttons_mode);
        
        text_artist    = (TextView)findViewById(R.id.text_artist);
        text_trackname = (TextView)findViewById(R.id.text_trackname);
        text_cur_time  = (TextView)findViewById(R.id.text_cur_time);
        text_full_time = (TextView)findViewById(R.id.text_full_time);
        seek_time      = (SeekBar) findViewById(R.id.seek_time);
        list_playlist  = (ListView)findViewById(R.id.list_playlist);
        
        button_playpause     = (ImageButton)findViewById(R.id.button_playpause);
        button_mode_normal   = (ImageButton)findViewById(R.id.button_mode_normal);
        button_mode_loop     = (ImageButton)findViewById(R.id.button_mode_loop);
        button_mode_random   = (ImageButton)findViewById(R.id.button_mode_random);
        button_mode_one      = (ImageButton)findViewById(R.id.button_mode_one);
        button_mode_loop_one = (ImageButton)findViewById(R.id.button_mode_loop_one);
        
        seek_time.setOnSeekBarChangeListener(this);
        
        image_spector = new SurfaceView(this);
        _callbacks = new MainActivityCallbacks(this);
        ((FrameLayout)findViewById(R.id.image_spector)).addView(image_spector);
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        
        Intent ps_intent = new Intent(this, StopPlayer.class);
        startService(ps_intent); bindService(ps_intent, this, BIND_AUTO_CREATE);
    }
    
    @Override
    public void onPause()
    {
        onServiceDisconnected(null); unbindService(this);
        super.onPause();
    }
    
    @Override
    public void onDestroy()
    {
        _callbacks.destroy(); _callbacks = null;
        image_spector = null;
        menu_main_track_search = null;
        
        seek_time.setOnSeekBarChangeListener(null);
        super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item;
        
        if((item = menu.findItem(R.id.menu_playlist)) != null)
            item.setIntent(new Intent(this, ListOfPlaylistsActivity.class));
        
        if((item = menu.findItem(R.id.menu_effects)) != null)
            item.setIntent(new Intent(this, EffectsActivity.class));
        
        if((item = menu.findItem(R.id.menu_import_export)) != null)
            item.setIntent(new Intent(this, ImportExportActivity.class));
        
        menu_main_track_search = (SearchView)menu.findItem(R.id.menu_main_track_search)
                                                 .getActionView();
        menu_main_track_search.setOnQueryTextFocusChangeListener(this);
        if(playlist_adapter != null)
            menu_main_track_search.setOnQueryTextListener(playlist_adapter);
        
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public void onServiceConnected(ComponentName name, IBinder binder)
    {
        SP = getService(binder);
        if(SP == null) return;
        
        SP.addFeedback(_callbacks, true);
        switch(SP.Storage.currentMode)
        {
            case NORMAL:   button_mode_normal.setBackgroundResource(R.color.buttons_hili); return;
            case LOOP:     button_mode_loop.setBackgroundResource(R.color.buttons_hili);   return;
            case RANDOM:   button_mode_random.setBackgroundResource(R.color.buttons_hili); return;
            case ONE:      button_mode_one.setBackgroundResource(R.color.buttons_hili);    return;
            case LOOP_ONE: button_mode_loop_one.setBackgroundResource(R.color.buttons_hili);
        }
    }
    
    @Override
    public void onServiceDisconnected(ComponentName name)
    {
        if(playlist_adapter != null)
        {
            if(! is_playlist_saved)
            {
                SP.Storage.savePlayback(); is_playlist_saved = true;
            }
            
            if(menu_main_track_search != null)
                menu_main_track_search.setOnQueryTextListener(null);
            
            playlist_adapter.destroy(); playlist_adapter = null;
        }
        
        if(SP != null)
        {
            SP.removeFeedback(_callbacks); SP = null;
        }
    }
    
    
    // -*- GUI handlers -*-
    
    public void on_button_playpause(View v)
    {   if(SP != null) SP.PlayPause();   }
    
    public void on_button_stop(View v)
    {   if(SP != null) SP.Stop();   }
    
    public void on_button_prev(View v)
    {   if(SP != null) SP.Previous();   }
    
    public void on_button_next(View v)
    {   if(SP != null) SP.Next();   }
    
    public void on_button_mode(View v)
    {
        if(SP == null) return;
        
        if(button_mode_normal == v) SP.Storage.currentMode = PlayingMode.NORMAL;
        else button_mode_normal.setBackgroundResource(R.color.transparent);
        
        if(button_mode_loop == v) SP.Storage.currentMode = PlayingMode.LOOP;
        else button_mode_loop.setBackgroundResource(R.color.transparent);
        
        if(button_mode_random == v) SP.Storage.currentMode = PlayingMode.RANDOM;
        else button_mode_random.setBackgroundResource(R.color.transparent);
        
        if(button_mode_one == v) SP.Storage.currentMode = PlayingMode.ONE;
        else button_mode_one.setBackgroundResource(R.color.transparent);
        
        if(button_mode_loop_one == v) SP.Storage.currentMode = PlayingMode.LOOP_ONE;
        else button_mode_loop_one.setBackgroundResource(R.color.transparent);
        
        SP.Storage.savePlayback();
        v.setBackgroundResource(R.color.buttons_hili);
    }
    
    public void on_rewind(View v)
    {
        if(SP != null) SP.setTime(SP.getTime() - TIME_SKIP_DELTA);
    }
    
    public void on_fast_forward(View v)
    {
        if(SP != null) SP.setTime(SP.getTime() + TIME_SKIP_DELTA);
    }
    
    @Override
    public void onItemSkip(long item_id)
    {
        playlist_adapter.getPlaylist().flipSkip((int)item_id);
        playlist_adapter.notifyDataSetChanged();
        is_playlist_saved = false;
    }
    
    @Override
    public void onItemClick(long item_id)
    {
        if(SP != null) SP.start((int)item_id);
    }
    
    @Override
    public void onItemDoc(long item_id) { /* holded */ }
    
    @Override
    public void onProgressChanged(SeekBar v, int progress, boolean fromUser) { /* holded */ }
    
    @Override
    public void onStartTrackingTouch(SeekBar v)
    {   _callbacks.is_scroll = false;   }
    
    @Override
    public void onStopTrackingTouch(SeekBar v)
    {
        if(SP != null) SP.setTime(seek_time.getProgress());
        _callbacks.is_scroll = true;
    }
    
    @Override
    public void onFocusChange(View v, boolean hasFocus) // for search menu
    {
        int visibility = hasFocus ? View.GONE : View.VISIBLE;
        layout_info.setVisibility(visibility);
        layout_seek.setVisibility(visibility);
        layout_buttons_play.setVisibility(visibility);
        layout_buttons_mode.setVisibility(visibility);
        
        if(! hasFocus)
        {
            layout_info.setAlpha(.0f);
            layout_info.animate().alpha(1.0f);
            layout_seek.setAlpha(.0f);
            layout_seek.animate().alpha(1.0f);
            layout_buttons_play.setAlpha(.0f);
            layout_buttons_play.animate().alpha(1.0f);
            layout_buttons_mode.setAlpha(.0f);
            layout_buttons_mode.animate().alpha(1.0f);
        }
    }
}
