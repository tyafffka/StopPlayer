package bred_soft.stop_player;

import java.util.TreeSet;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

//import com.mobeta.android.dslv.DragSortListView;
import bred_soft.utils.ListButtonsClicker;

import static bred_soft.stop_player.storage.MusicStorage.Playlist;
import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.utils.PositionList.ListIterator;
import static bred_soft.utils.ListButtonsClicker.OnListButtonClickListener;



@SuppressWarnings("WeakerAccess")
public class PlaylistAdapter extends BaseAdapter
    implements OnListButtonClickListener, SearchView.OnQueryTextListener /*,
    DragSortListView.DropListener*/
{
    interface ClickListener
    {
        void onItemSkip(long item_id);
        void onItemClick(long item_id);
        void onItemDoc(long item_id);
    }
    
    public static final short TAG_ITEM_SKIP = 0;
    public static final short TAG_ITEM = 10;
    public static final short TAG_ITEM_DOC = 20;
    
    protected static String EMPTY_TIME = null;
    
    protected boolean _for_view;
    protected int _selected;
    protected Playlist _playlist;
    protected ListIterator<Item> _last_item_p;
    
    private ListButtonsClicker __clicker;
    private TreeSet<Integer> __filtered = new TreeSet<>();
    
    protected ClickListener _listener;
    protected LayoutInflater _inflater;
    protected ListView _list;
    
    
    public PlaylistAdapter(Activity activity_, ClickListener listener_, ListView list_,
                           Playlist playlist_, boolean for_view_)
    {
        if(EMPTY_TIME == null)
            EMPTY_TIME = activity_.getString(R.string.empty_time);
        
        _for_view = for_view_; _selected = -1;
        _playlist = playlist_;
        _last_item_p = _playlist.iterator(0);
        
        __clicker = new ListButtonsClicker(this);
        _listener = listener_;
        
        _inflater = activity_.getLayoutInflater();
        _list = list_; _list.setAdapter(this); /*_list.setDropListener(this);*/
        
        notifyDataSetChanged();
    }
    
    public void destroy()
    {
        _inflater = null;
        _list.setAdapter(null); /*_list.setDropListener(null);*/ _list = null;
        
        __clicker.destroy(); __clicker = null;
        _listener = null;
        
        _playlist = null;
        _last_item_p = null;
    }
    
    public void select(int position_)
    {
        _list.smoothScrollToPosition(_selected = position_);
        notifyDataSetChanged();
    }
    
    /*@Override
    public void drop(int from_, int to_)
    {
        if(_selected == from_) _selected = to_;
        
        if(from_ < to_) ++to_;
        _playlist.move(from_, to_);
        
        notifyDataSetChanged();
    }*/
    
    @Override
    public void onListButtonClick(int position_, short tag_)
    {
        long id = getItemId(position_);
        switch(tag_)
        {
        case TAG_ITEM_SKIP: _listener.onItemSkip(id); return;
        case TAG_ITEM: _listener.onItemClick(id); return;
        case TAG_ITEM_DOC: _listener.onItemDoc(id);
        }
    }
    
    @Override
    public boolean onQueryTextChange(String query_)
    {   return onQueryTextSubmit(query_);   }
    
    @Override
    public boolean onQueryTextSubmit(String query_)
    {
        __filtered.clear();
        
        if(! query_.isEmpty())
        {
            boolean is_found = false;
            query_ = query_.toLowerCase();
            
            for(ListIterator<Item> it = _playlist.iterator(0);
                (! it.isEnd()); it.next())
            {
                Item item = it.get();
                if((! item.artist.toLowerCase().contains(query_)) &&
                   (! item.trackname.toLowerCase().contains(query_))) continue;
                
                if(! is_found)
                {
                    _list.smoothScrollToPosition(it.position()); is_found = true;
                }
                __filtered.add(it.position());
            }
        }
        
        notifyDataSetChanged();
        return true;
    }
    
    
    // -*- Adapter methods -*-
    
    public Playlist getPlaylist() {   return _playlist;   }
    
    @Override
    public int getCount() {   return _playlist.size();   }
    
    @Override
    public Item getItem(int position_)
    {
        __move_item_p:
        {
            int diff = position_ - _last_item_p.position();
            
            if(diff == 0)
            {   break __move_item_p;   }
            if(diff > 0)
            {
                if(diff < _playlist.size() - position_)
                {
                    do {   _last_item_p.next();   }
                    while(_last_item_p.position() < position_);
                    break __move_item_p;
                }
            }
            else if(-diff < position_)
            {
                do {   _last_item_p.prev();   }
                while(_last_item_p.position() > position_);
                break __move_item_p;
            }
            
            _last_item_p = _playlist.iterator(position_);
        }
        return _last_item_p.get();
    }
    
    @Override
    public long getItemId(int position_)
    {
        return (0 <= position_ && position_ < _playlist.size())?
               position_ : -1;
    }
    
    @Override
    public View getView(int position_, View row_, ViewGroup parent_)
    {
        if(row_ == null)
        {
            row_ = _inflater.inflate((_for_view ? R.layout.playlist_view_item :
                                      R.layout.playlist_item), parent_, false);
            if(row_ == null) return null;
        }
        
        Item item = getItem(position_);
        
        if(position_ == _selected)
            row_.setBackgroundResource(R.color.track_hili);
        else if(__filtered.contains(position_))
            row_.setBackgroundResource(R.color.search_hili);
        else
            row_.setBackgroundResource(R.color.transparent);
        
        ImageButton button_track_skip = (ImageButton)row_.findViewById(R.id.button_track_skip);
        if(item.skip)
            button_track_skip.setImageResource(R.drawable.ic_state_mute);
        else
            button_track_skip.setImageResource(R.drawable.ic_state_unmute);
        
        __clicker.setOnClick(button_track_skip, position_, TAG_ITEM_SKIP);
        __clicker.setOnClick(row_.findViewById(R.id.playlist_view_main), position_, TAG_ITEM);
        if(_for_view)
            __clicker.setOnClick(row_.findViewById(R.id.button_track_dialog), position_, TAG_ITEM_DOC);
        
        ((TextView)row_.findViewById(R.id.pli_artist)   ).setText(item.artist);
        ((TextView)row_.findViewById(R.id.pli_trackname)).setText(item.trackname);
        ((TextView)row_.findViewById(R.id.pli_duration) ).setText(getTimeString(item.duration));
        return row_;
    }
    
    
    // -*- internal used -*-
    
    public static String getTimeString(int time_)
    {
        if(time_ < 0) return EMPTY_TIME;
        
        time_ /= 1000;
        int minutes = time_ / 60, seconds = time_ % 60;
        return minutes + ((seconds < 10)? ":0" : ":") + seconds;
    }
}
