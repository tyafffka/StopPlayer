package bred_soft.stop_player.storage;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import bred_soft.utils.INIFile;



@SuppressWarnings("WeakerAccess")
class LocalFilesStorage extends ListOfPlaylistsStorage
{
    protected HashMap<String, Boolean> _skips_cache = new HashMap<>();
    protected ContentResolver _content_resolver;
    
    
    LocalFilesStorage(Context context_)
    {
        super(context_);
        _content_resolver = context_.getContentResolver();
        
        __load_skips();
        __load_local_files_playlists();
        _recache_playlists_order();
    }
    
    @Override
    public void destroy()
    {
        __save_skips();
        _skips_cache.clear(); _skips_cache = null;
        
        _content_resolver = null;
        super.destroy();
    }
    
    
    private void __load_skips()
    {
        INIFile f = new INIFile();
        try {   f.read(FILES_PREFIX + SKIPS_FILE);   }
        catch(IOException e) {   return;   }
        
        for(Map.Entry<String, String> pair : f.section("Tracks").entrySet())
            _skips_cache.put(pair.getKey(), Boolean.parseBoolean(pair.getValue()));
    }
    
    private void __save_skips()
    {
        INIFile f = new INIFile();
        
        INIFile.Section s = f.section("Tracks");
        for(Map.Entry<String, Boolean> pair : _skips_cache.entrySet())
            s.put(pair.getKey(), pair.getValue().toString());
        
        try {   f.write(FILES_PREFIX + SKIPS_FILE);   }
        catch(IOException ignored) {}
    }
    
    private void __load_local_files_playlists()
    {
        Cursor cursor = _content_resolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            new String[]{
                MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DURATION
            },
            null, null,
            MediaStore.Audio.Media.TRACK + " ASC"
        );
        if(cursor == null) return;
        
        if(! cursor.moveToFirst())
        {   cursor.close(); return;   }
        
        int iuri = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
        int iartist = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int itrackname = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
        int iduration = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
        
        TreeMap<String, Playlist> folders = new TreeMap<>();
        do // collect files
        {
            Item item = new Item();
            item.uri = cursor.getString(iuri);
            item.artist = cursor.getString(iartist);
            item.trackname = cursor.getString(itrackname);
            item.duration = cursor.getInt(iduration);
            item.skip = _skips_cache.containsKey(item.uri) ?
                        _skips_cache.get(item.uri) : false;
            
            if(item.artist == null || item.artist.isEmpty() ||
               item.artist.equals("<unknown>")) item.artist = EMPTY_ARTIST;
            
            if(item.trackname == null || item.trackname.isEmpty() ||
               item.trackname.equals("<unknown>")) item.trackname = EMPTY_TRACKNAME;
            
            if(item.duration <= 0) item.duration = -1;
            
            int i = item.uri.lastIndexOf('/');
            String folder = (i > 0)? item.uri.substring(0, i) : "";
            
            Playlist pl;
            if(folders.containsKey(folder))
                pl = folders.get(folder);
            else
                folders.put(folder, pl = new Playlist());
            pl.add(item);
        }
        while(cursor.moveToNext());
        
        // collect folders
        for(Map.Entry<String, Playlist> pair : folders.entrySet())
        {
            String folder = pair.getKey();
            String name;
            
            int i = folder.length();
            for(;;)
            {
                i = folder.lastIndexOf('/', i - 1);
                if(i <= 0)
                {
                    name = folder + "/"; break;
                }
                
                name = folder.substring(i + 1) + "/";
                if(! _playlists.containsKey(name)) break;
            }
            
            _LocalFilesPlaylistStoring storing = new _LocalFilesPlaylistStoring();
            storing.playlist = pair.getValue();
            _playlists.put(name, storing);
        }
        
        cursor.close();
    }
    
    
    // -*- * -*-
    
    protected class _LocalFilesPlaylistStoring implements _PlaylistStoring
    {
        public Playlist playlist = null;
        
        @Override
        public boolean name_check(String name_)
        {   return name_.endsWith("/");   }
        
        @Override
        public Playlist load(String name_)
        {   return playlist;   }
        
        @Override
        public boolean save(String name_, Playlist pl_)
        {
            playlist = pl_;
            
            for(Playlist.ListIterator<Item> it = pl_.iterator(0);
                (! it.isEnd()); it.next())
            {
                Item item = it.get();
                _skips_cache.put(item.uri, item.skip);
            }
            return true;
        }
        
        @Override
        public boolean remove(String name_)
        {   playlist = null; return true;   }
        
        @Override
        public boolean rename(String from_name_, String to_name_)
        {   return true;   }
    }
}
