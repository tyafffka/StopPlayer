package bred_soft.stop_player.storage;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import android.content.Context;

import bred_soft.stop_player.R;



@SuppressWarnings({"WeakerAccess", "BooleanMethodIsAlwaysInverted"})
class BaseStorage
{
    public static String EMPTY_ARTIST = null;
    public static String EMPTY_TRACKNAME = null;
    
    public static String FILES_PREFIX = null;
    public static final String PLAYBACK_FILE = "playback.ini";
    public static final String EFFECTS_FILE = "effects.ini";
    public static final String PLAYLIST_EXT = ".playlist.ini";
    public static final String SKIPS_FILE = "skips.ini";
    
    private File __internal_dir;
    
    
    BaseStorage(Context context_)
    {
        if(EMPTY_ARTIST == null)
            EMPTY_ARTIST = context_.getString(R.string.empty_artist);
        if(EMPTY_TRACKNAME == null)
            EMPTY_TRACKNAME = context_.getString(R.string.empty_trackname);
        
        __internal_dir = context_.getFilesDir();
        
        if(FILES_PREFIX == null)
            FILES_PREFIX = __internal_dir.getAbsolutePath() + File.separator;
    }
    
    public void destroy()
    {   __internal_dir = null;   }
    
    
    /* Копирует содержимое файла `from_` в файл `to_`.
     * Возвращает, успешно ли завершилось копирование. */
    public boolean copyFile(File from_, File to_)
    {
        try
        {
            if(to_.exists()) if(! to_.delete()) return false;
        }
        catch(SecurityException e) {   return false;   }
        try
        {
            FileChannel in = new FileInputStream(from_).getChannel();
            FileChannel out = new FileOutputStream(to_).getChannel();
            
            out.transferFrom(in, 0, in.size());
            return true;
        }
        catch(IOException e) {   return false;   }
    }
    
    /* Возвращает список имён файлов плейлистов (без пути),
     * находящихся во внутреннем хранилище. */
    public ArrayList<String> getPlaylistFiles()
    {
        ArrayList<String> r = new ArrayList<>();
        for(File f : __internal_dir.listFiles())
        {
            if(! f.isFile()) continue;
            
            String name = f.getName();
            if(name.endsWith(PLAYLIST_EXT)) r.add(name);
        }
        return r;
    }
    
    /* Экспортирует все файлы настроек из внутреннего хранилища
     * приложения в указанную папку во внешнем хранилище.
     * Возвращает успешность операции. */
    public boolean exportFiles(File external_dir_)
    {
        if(! external_dir_.canWrite()) return false;
        File f;
        
        /*if((f = new File(__internal_dir, PLAYBACK_FILE)).exists())
            if(! copyFile(f, new File(external_dir_, PLAYBACK_FILE))) return false;*/
        
        if((f = new File(__internal_dir, EFFECTS_FILE)).exists())
            if(! copyFile(f, new File(external_dir_, EFFECTS_FILE))) return false;
        
        if((f = new File(__internal_dir, SKIPS_FILE)).exists())
            if(! copyFile(f, new File(external_dir_, SKIPS_FILE))) return false;
        
        for(File fp : __internal_dir.listFiles())
        {
            if(! fp.isFile()) continue;
            
            String name = fp.getName();
            if(name.endsWith(PLAYLIST_EXT))
                if(! copyFile(fp, new File(external_dir_, name))) return false;
        }
        return true;
    }
    
    /* Импортирует файлы настроек из указанной папки внешнего хранилища
     * во внутреннее хранилище приложения. Возвращает успешность операции. */
    public boolean importFiles(File external_dir_)
    {
        if(! external_dir_.canRead()) return false;
        File f;
        
        /*if((f = new File(external_dir_, PLAYBACK_FILE)).exists())
            if(! copyFile(f, new File(__internal_dir, PLAYBACK_FILE))) return false;*/
        
        if((f = new File(external_dir_, EFFECTS_FILE)).exists())
            if(! copyFile(f, new File(__internal_dir, EFFECTS_FILE))) return false;
        
        if((f = new File(external_dir_, SKIPS_FILE)).exists())
            if(! copyFile(f, new File(__internal_dir, SKIPS_FILE))) return false;
        
        for(File fp : external_dir_.listFiles())
        {
            if(! fp.isFile()) continue;
            
            String name = fp.getName();
            if(name.endsWith(PLAYLIST_EXT))
                if(! copyFile(fp, new File(__internal_dir, name))) return false;
        }
        return true;
    }
}
