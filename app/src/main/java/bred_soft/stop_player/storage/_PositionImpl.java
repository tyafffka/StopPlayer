package bred_soft.stop_player.storage;

import java.util.Random;

import bred_soft.utils.PositionList;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.utils.PositionList.ListIterator;



@SuppressWarnings("WeakerAccess")
class _PositionImpl
{
    private static Random GENERATOR = null;
    
    private PositionList<Item> __playlist;
    private ListIterator<Item> __current;
    private PositionList<ListIterator<Item>> __shuffled = null;
    private ListIterator<ListIterator<Item>> __current_shuffled = null;
    
    
    public _PositionImpl(PositionList<Item> playlist_)
    {
        if(GENERATOR == null)
        {
            GENERATOR = new Random();
            GENERATOR.setSeed(System.currentTimeMillis());
        }
        
        __playlist = playlist_;
        __current = playlist_.iterator(-1);
    }
    
    public void destroy()
    {
        __playlist = null;
        __current = null;
        
        if(__shuffled != null)
        {
            __shuffled.destroy(); __shuffled = null;
        }
        __current_shuffled = null;
    }
    
    
    public ListIterator<Item> get()
    {   return __current;   }
    
    public void set(ListIterator<Item> item_p_)
    {   __current = item_p_;   }
    
    public boolean next()
    {
        boolean no_loop = true;
        for(ListIterator<Item> item_p = new ListIterator<Item>(__current);;)
        {
            item_p.next();
            
            if(item_p.equals(__current)) return false;
            else if(item_p.isEnd()) no_loop = false;
            else if(! item_p.get().skip)
            {
                set(item_p); return no_loop;
            }
        }
    }
    
    public boolean prev()
    {
        boolean no_loop = true;
        for(ListIterator<Item> item_p = new ListIterator<Item>(__current);;)
        {
            item_p.prev();
            
            if(item_p.equals(__current)) return false;
            else if(item_p.isEnd()) no_loop = false;
            else if(! item_p.get().skip)
            {
                set(item_p); return no_loop;
            }
        }
    }
    
    public boolean nextRandom()
    {
        if(__playlist.size() < 3) return next();
        
        if(__shuffled == null || __current_shuffled == null)
            __generate_shuffle();
        
        boolean from_start =(
            __current_shuffled.isEnd() || (__current_shuffled.position() == 0)
        );
        for(;;)
        {
            __current_shuffled.next();
            if(__current_shuffled.isEnd())
            {
                if(from_start) return false;
                
                __generate_shuffle(); from_start = true;
                continue;
            }
            
            ListIterator<Item> item_p = __current_shuffled.get();
            if(! item_p.get().skip)
            {
                set(item_p); return true;
            }
        }
    }
    
    public boolean prevRandom()
    {
        if(__playlist.size() < 3) return prev();
        
        if(__shuffled == null || __current_shuffled == null ||
           __current_shuffled.isEnd())
        {   return false;   }
        
        for(ListIterator<ListIterator<Item>> item_pp =
                new ListIterator<ListIterator<Item>>(__current_shuffled);;)
        {
            item_pp.prev();
            if(item_pp.isEnd()) return false;
            
            ListIterator<Item> item_p = item_pp.get();
            if(! item_p.get().skip)
            {
                __current_shuffled = item_pp; set(item_p);
                return true;
            }
        }
    }
    
    
    private void __generate_shuffle()
    {
        int size = __playlist.size();
        int curr = __current.isEnd() ? -1 : __current.position();
        int shuffle_start = (curr >= 0)? 1 : 0;
        
        // Shuffle
        
        int[] positions = new int[size];
        for(int i = shuffle_start; i < size; ++i) positions[i] = i;
        
        if(curr >= 0)
        {
            positions[0] = curr; positions[curr] = 0;
        }
        
        for(int i = size - 1; i > shuffle_start; --i)
        {
            int swap = (curr >= 0)? GENERATOR.nextInt(i) + 1 : GENERATOR.nextInt(i + 1);
            if(swap == i) continue;
            
            int tmp = positions[swap];
            positions[swap] = positions[i];
            positions[i] = tmp;
        }
        
        // Fill list
        
        if(__shuffled != null) __shuffled.destroy();
        __shuffled = new PositionList<ListIterator<Item>>();
        
        ListIterator<Item> item_p = __playlist.iterator(0);
        for(int pos : positions)
        {
            __move_item_p:
            {
                int diff = pos - item_p.position();
                
                if(diff == 0)
                {   break __move_item_p;   }
                if(diff > 0)
                {
                    if(diff < size - pos)
                    {
                        do {   item_p.next();   }
                        while(item_p.position() < pos);
                        break __move_item_p;
                    }
                }
                else if(-diff < pos)
                {
                    do {   item_p.prev();   }
                    while(item_p.position() > pos);
                    break __move_item_p;
                }
                
                item_p = __playlist.iterator(pos);
            }
            __shuffled.add(new ListIterator<Item>(item_p));
        } //- foreach(position)
        
        __current_shuffled = __shuffled.iterator(shuffle_start - 1);
    } //- __generate_shuffle
}
