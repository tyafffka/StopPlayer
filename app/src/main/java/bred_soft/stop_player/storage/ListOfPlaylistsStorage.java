package bred_soft.stop_player.storage;

import java.io.File;
import java.io.IOException;
import java.util.TreeMap;

import android.content.Context;

import bred_soft.utils.INIFile;

import static java.util.Arrays.binarySearch;



@SuppressWarnings({"WeakerAccess", "BooleanMethodIsAlwaysInverted"})
class ListOfPlaylistsStorage extends PlaybackStorage
{
    protected interface _PlaylistStoring
    {
        boolean name_check(String name_);
        Playlist load(String name_);
        boolean save(String name_, Playlist pl_);
        boolean remove(String name_);
        boolean rename(String from_name_, String to_name_);
    }
    
    protected _RegularPlaylistStoring _regular_storing = new _RegularPlaylistStoring();
    protected TreeMap<String, _PlaylistStoring> _playlists = new TreeMap<>();
    protected String[] _playlists_order;
    
    
    ListOfPlaylistsStorage(Context context_)
    {
        super(context_);
        __load_regular_playlists();
        _recache_playlists_order();
    }
    
    @Override
    public void destroy()
    {
        _playlists.clear(); _playlists = null;
        _playlists_order = null;
        
        _regular_storing = null;
        super.destroy();
    }
    
    /* Возвращает количество плейлистов в хранилище. */
    public int playlistsCount()
    {   return _playlists.size();   }
    
    /* Возвращает массив названий плейлистов в хранилище. */
    public String[] getPlaylistsNames()
    {   return _playlists_order;   }
    
    /* Возвращает название плейлиста по его номеру. */
    public String getPlaylistName(int i_)
    {
        if(i_ < 0 || _playlists.size() <= i_) return null;
        return _playlists_order[i_];
    }
    
    protected void _recache_playlists_order()
    {   _playlists_order = _playlists.keySet().toArray(new String[0]);   }
    
    
    // -*- Playlists methods -*-
    
    /* Добавляет обычный плейлист с указанным названием в хранилище.
     * В случае успеха возвращает номер нового плейлиста, иначе -1. */
    public int addPlaylist(String name_)
    {
        if(_playlists.containsKey(name_)) return -1;
        if(! _regular_storing.name_check(name_)) return -1;
        
        _regular_storing.save(name_, new Playlist());
        
        _playlists.put(name_, _regular_storing);
        _recache_playlists_order();
        return binarySearch(_playlists_order, name_);
    }
    
    /* Возвращает объект плейлиста по его номеру. */
    public Playlist getPlaylist(int i_)
    {
        if(i_ < 0 || _playlists.size() <= i_) return null;
        
        String name = _playlists_order[i_];
        return _playlists.get(name).load(name);
    }
    
    /* Записывает плейлист в хранилище на место, указанное номером. */
    public boolean setPlaylist(int i_, Playlist pl_)
    {
        if(i_ < 0 || _playlists.size() <= i_) return false;
        
        String name = _playlists_order[i_];
        return _playlists.get(name).save(name, pl_);
    }
    
    /* Удаляет плейлист из хранилища по номеру. */
    public boolean removePlaylist(int i_)
    {
        if(i_ < 0 || _playlists.size() <= i_) return false;
        
        String name = _playlists_order[i_];
        if(! _playlists.get(name).remove(name)) return false;
        
        _playlists.remove(name);
        _recache_playlists_order();
        return true;
    }
    
    /* Переименовывает плейлист с указанным номером к указанному имени.
     * Возвращает новый номер плейлиста в случае успеха, иначе -1. */
    public int renamePlaylist(int from_i_, String to_name_)
    {
        if(from_i_ < 0 || _playlists.size() <= from_i_) return -1;
        if(_playlists.containsKey(to_name_)) return -1;
        
        String from_name = _playlists_order[from_i_];
        _PlaylistStoring storing = _playlists.get(from_name);
        
        if(! storing.name_check(to_name_)) return -1;
        if(! storing.rename(from_name, to_name_)) return -1;
        
        _playlists.remove(from_name); _playlists.put(to_name_, storing);
        _recache_playlists_order();
        return binarySearch(_playlists_order, to_name_);
    }
    
    /* Копирует содержимое плейлиста с указанным номером в новый
     * плейлист с указанным названием. Возвращает номер созданного
     * плейлиста в случае успеха, иначе -1. */
    public int copyPlaylist(int from_i_, String to_name_)
    {
        if(from_i_ < 0 || _playlists.size() <= from_i_) return -1;
        if(_playlists.containsKey(to_name_)) return -1;
        if(! _regular_storing.name_check(to_name_)) return -1;
        
        String from_name = _playlists_order[from_i_];
        if(! _regular_storing.save(to_name_, _playlists.get(from_name).load(from_name)))
            return -1;
        
        _playlists.put(to_name_, _regular_storing);
        _recache_playlists_order();
        return binarySearch(_playlists_order, to_name_);
    }
    
    /* Добавляет содержимое плейлиста с номером `from_` в конец плейлиста
     * с номером `to_`. Возвращает успешность операции. */
    public boolean mergePlaylists(int to_, int from_)
    {
        Playlist pl_to = getPlaylist(to_), pl_from = getPlaylist(from_);
        if(pl_to == null || pl_from == null) return false;
        
        pl_to.addAll(pl_from);
        return setPlaylist(to_, pl_to);
    }
    
    
    // -*- Regular storing -*-
    
    private void __load_regular_playlists()
    {
        for(String name : getPlaylistFiles())
            _playlists.put(name.substring(0, name.length() - PLAYLIST_EXT.length()),
                           _regular_storing);
    }
    
    protected class _RegularPlaylistStoring implements _PlaylistStoring
    {
        @Override @SuppressWarnings("ResultOfMethodCallIgnored")
        public boolean name_check(String name_)
        {
            if(name_ == null || name_.isEmpty()) return false;
            if(name_.contains("/") || name_.contains("\\")) return false;
            
            File f = new File(name_);
            try
            {   f.getCanonicalPath(); return true;   }
            catch(IOException e)
            {   return false;   }
        }
        
        @Override
        public Playlist load(String name_)
        {
            try
            {   return loadPlaylist(new INIFile(FILES_PREFIX + name_ + PLAYLIST_EXT));   }
            catch(IOException e)
            {   return null;   }
        }
        
        @Override
        public boolean save(String name_, Playlist pl_)
        {
            if(pl_ == null) return false;
            try
            {
                savePlaylist(pl_).write(FILES_PREFIX + name_ + PLAYLIST_EXT);
                return true;
            }
            catch(IOException e)
            {   return false;   }
        }
        
        @Override
        public boolean remove(String name_)
        {
            File f = new File(FILES_PREFIX + name_ + PLAYLIST_EXT);
            return(f.exists() && f.delete());
        }
        
        @Override
        public boolean rename(String from_name_, String to_name_)
        {
            File f_from = new File(FILES_PREFIX + from_name_ + PLAYLIST_EXT);
            if(! f_from.exists()) return false;
            
            File f_to = new File(FILES_PREFIX + to_name_ + PLAYLIST_EXT);
            if(f_to.exists()) if(! f_to.delete()) return false;
            
            return f_from.renameTo(f_to);
        }
    } //- class _RegularPlaylistStoring
}
