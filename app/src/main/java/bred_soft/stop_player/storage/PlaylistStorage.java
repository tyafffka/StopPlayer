package bred_soft.stop_player.storage;

import android.content.Context;
import android.media.MediaMetadataRetriever;

import bred_soft.utils.INIFile;
import bred_soft.utils.PositionList;



@SuppressWarnings("WeakerAccess")
class PlaylistStorage extends BaseStorage
{
    /* Класс, представляющий метаданные одного трека. */
    public static class Item
    {
        public String uri;
        public String artist;
        public String trackname;
        public int duration;
        public boolean skip = false;
        
        public Item()
        {
            uri = "";
            artist = EMPTY_ARTIST;
            trackname = EMPTY_TRACKNAME;
            duration = -1;
        }
        
        /* Создаёт трек по указанному адресу (локальному или по ссылке).
         * Извлекает метаданные из файла. */
        public Item(String uri_)
        {
            uri = uri_;
            if(uri == null) return;
            
            // get metadata from URI
            MediaMetadataRetriever MMR = new MediaMetadataRetriever();
            MMR.setDataSource(uri);
            
            artist = MMR.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            trackname = MMR.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            duration = Integer.decode(
                MMR.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
            );
            MMR.release();
            
            if(artist == null || artist.isEmpty())
                artist = EMPTY_ARTIST;
            
            if(trackname == null || trackname.isEmpty())
            {
                String path = uri;
                
                int i;
                i = path.lastIndexOf("/"); if(i >= 0) path = path.substring(i + 1);
                i = path.lastIndexOf("."); if(i >= 0) path = path.substring(0, i);
                
                trackname = path;
            }
        }
        
        public Item copy()
        {
            Item cp = new Item();
            cp.uri = uri;
            cp.artist = artist;
            cp.trackname = trackname;
            cp.duration = duration;
            return cp;
        }
    } //- class Item
    
    
    /* Режим воспроизведения. Определяет поведение плеера,
     * когда воспроизведение текущего трека заканчивается. */
    public enum PlayingMode
    {   NORMAL, LOOP, RANDOM, ONE, LOOP_ONE   }
    
    
    /* Класс, представляющий плейлист.
     * Опционально обрабатывает текущий трек. */
    public static class Playlist extends PositionList<Item>
    {
        protected _PositionImpl _pos_impl = null;
        
        
        public Playlist() {   super();   }
        
        public void destroy()
        {
            if(_pos_impl != null)
            {
                _pos_impl.destroy(); _pos_impl = null;
            }
            super.destroy();
        }
        
        public Playlist copy()
        {
            Playlist cp = new Playlist();
            cp.addAll(this);
            return cp;
        }
        
        
        /* Создаёт трек по указанному адресу и добавляет его в плейлист. */
        public void add(int i_, String uri_)
        {   super.add(i_, new Item(uri_));   }
        
        /* Меняет метку пропуска трека. */
        public void flipSkip(int i_)
        {   get(i_).skip ^= true;   }
        
        /* Возвращает номер текущего трека. */
        public int getPosition()
        {
            if(_pos_impl == null) _pos_impl = new _PositionImpl(this);
            
            ListIterator<Item> it = _pos_impl.get();
            return it.isEnd() ? -1 : it.position();
        }
        
        /* Возвращает объект текущего трека. */
        public Item getCurrentItem()
        {
            if(_pos_impl == null) _pos_impl = new _PositionImpl(this);
            return _pos_impl.get().get();
        }
        
        /* Возвращает адрес текущего трека. */
        public String getCurrentURI()
        {
            if(_pos_impl == null) _pos_impl = new _PositionImpl(this);
            
            ListIterator<Item> it = _pos_impl.get();
            return it.isEnd() ? null : it.get().uri;
        }
        
        /* Выбирает указанный трек в качестве текущего.
         * Возвращает, указан ли валидный номер трека. */
        public boolean setPosition(int i_)
        {
            if(i_ < 0 || size() <= i_) return false;
            if(_pos_impl == null) _pos_impl = new _PositionImpl(this);
            
            _pos_impl.set(iterator(i_));
            return true;
        }
        
        /* Осуществляют переход к следующему треку
         * в соответствии с указанным режимом воспроизведения.
         * Возвращают, следует ли продолжать воспроизведение. */
        public boolean toNext(PlayingMode mode_, boolean manual_)
        {
            if(size() == 0) return false;
            if(_pos_impl == null) _pos_impl = new _PositionImpl(this);
            
            if(! manual_)
            {
                if(mode_ == PlayingMode.LOOP_ONE) return true;
                if(mode_ == PlayingMode.ONE) return false;
            }
            
            if(mode_ == PlayingMode.RANDOM)
            {
                boolean has_next = _pos_impl.nextRandom();
                return(manual_ || has_next);
            }
            else
            {
                boolean has_next = _pos_impl.next();
                return(manual_ || has_next || mode_ != PlayingMode.NORMAL);
            }
        }
        
        public boolean toNext(PlayingMode mode_)
        {   return toNext(mode_, true);   }
        
        /* Осуществляет переход к предыдущему треку
         * в соответствии с указанным режимом воспроизведения.
         * Возвращает, следует ли продолжать воспроизведение. */
        public boolean toPrevious(PlayingMode mode_)
        {
            if(size() == 0) return false;
            if(_pos_impl == null) _pos_impl = new _PositionImpl(this);
            
            if(mode_ == PlayingMode.RANDOM)
            {   return _pos_impl.prevRandom();   }
            else
            {
                boolean has_prev = _pos_impl.prev();
                return(has_prev || mode_ != PlayingMode.NORMAL);
            }
        }
    } //- class Playlist
    
    
    PlaylistStorage(Context context_) {   super(context_);   }
    
    
    /* Извлекает плейлист из указанного файла настроек.
     * Файл должен быть загружен. */
    public static Playlist loadPlaylist(INIFile f_)
    {
        Playlist pl = new Playlist();
        int count = Integer.decode(f_.section("Playlist").get("tracks", "0"));
        
        for(int i = 0; i < count; ++i)
        {
            INIFile.Section track = f_.section("Track" + i);
            String uri = track.get("uri", "");
            if(uri.isEmpty()) continue;
            
            Item item = new Item();
            item.uri = uri;
            item.artist = track.get("artist", EMPTY_ARTIST);
            item.trackname = track.get("trackname", EMPTY_TRACKNAME);
            item.duration = Integer.decode(track.get("duration", "-1"));
            item.skip = Boolean.parseBoolean(track.get("skip", "false"));
            
            pl.add(item);
        }
        return pl;
    }
    
    /* Создаёт файл настроек и помещает в него указанный плейлист.
     * Файл должен быть сохранён отдельно. */
    public static INIFile savePlaylist(Playlist pl_)
    {
        INIFile f = new INIFile();
        f.section("Playlist").put("tracks", String.valueOf(pl_.size()));
        
        for(PositionList.ListIterator<Item> it = pl_.iterator(0);
            (! it.isEnd()); it.next())
        {
            Item item = it.get();
            INIFile.Section track = f.section("Track" + it.position());
            
            track.put("uri", item.uri);
            track.put("artist", item.artist);
            track.put("trackname", item.trackname);
            track.put("duration", String.valueOf(item.duration));
            track.put("skip", String.valueOf(item.skip));
        }
        return f;
    }
    
    public static String playingModeToStr(PlayingMode m_)
    {
        switch(m_)
        {
        case NORMAL:   return "normal";
        case LOOP:     return "loop";
        case RANDOM:   return "random";
        case ONE:      return "one";
        case LOOP_ONE: return "loop_one";
        default:       return "";
        }
    }
    
    public static PlayingMode strToPlayingMode(String s_)
    {
        switch(s_)
        {
        default:
        case "normal":   return PlayingMode.NORMAL;
        case "loop":     return PlayingMode.LOOP;
        case "random":   return PlayingMode.RANDOM;
        case "one":      return PlayingMode.ONE;
        case "loop_one": return PlayingMode.LOOP_ONE;
        }
    }
}
