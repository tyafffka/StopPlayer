package bred_soft.stop_player.storage;

import java.io.IOException;
import java.util.TreeMap;

import android.content.Context;

import bred_soft.utils.INIFile;



@SuppressWarnings({"WeakerAccess", "RedundantSuppression"})
public class PlaybackStorage extends PlaylistStorage
{
    public Item clipboardTrack = null;
    
    public Playlist currentPlaylist = null;
    public int currentPosition = 0;
    public PlayingMode currentMode = PlayingMode.NORMAL;
    
    public int[] currentEqu = null;
    public int currentBassLevel = 0;
    public TreeMap<String, int[]> userEquPresets = new TreeMap<>();
    
    
    public PlaybackStorage(Context context_)
    {
        super(context_);
        __load_playback();
        __load_effects();
    }
    
    @Override
    public void destroy()
    {
        userEquPresets.clear(); userEquPresets = null;
        currentEqu = null;
        
        currentPlaylist = null; clipboardTrack = null;
        super.destroy();
    }
    
    
    private void __load_playback()
    {
        INIFile f = new INIFile();
        try {   f.read(FILES_PREFIX + PLAYBACK_FILE);   }
        catch(IOException e) {   return;   }
        
        currentPlaylist = loadPlaylist(f);
        currentPosition = Integer.decode(f.section("Playlist").get("position", "0"));
        currentMode = strToPlayingMode(f.section("Playlist").get("mode", ""));
        
    }
    
    public void savePlayback()
    {
        INIFile f = (currentPlaylist != null)?
                    savePlaylist(currentPlaylist) : new INIFile();
        f.section("Playlist").put("position", String.valueOf(currentPosition));
        f.section("Playlist").put("mode", playingModeToStr(currentMode));
        
        try {   f.write(FILES_PREFIX + PLAYBACK_FILE);   }
        catch(IOException ignored) {}
    }
    
    
    private int[] __load_equ(INIFile.Section sec_)
    {
        int count = Integer.decode(sec_.get("bands", "0"));
        if(count <= 0) return null;
        
        int[] r = new int[count];
        for(int k = 0; k < count; ++k)
            r[k] = Integer.decode(sec_.get("band" + k, "0"));
        return r;
    }
    
    private void __load_effects()
    {
        INIFile f = new INIFile();
        try {   f.read(FILES_PREFIX + EFFECTS_FILE);   }
        catch(IOException e) {   return;   }
        
        INIFile.Section effects = f.section("Effects");
        
        currentEqu = __load_equ(effects);
        currentBassLevel = Integer.decode(effects.get("bass", "0"));
        int count = Integer.decode(effects.get("presets", "0"));
        
        for(int i = 0; i < count; ++i)
        {
            INIFile.Section preset = f.section("Preset" + i);
            String name = preset.get("name", "");
            if(name.isEmpty()) continue;
            
            int[] equ = __load_equ(preset);
            if(equ != null) userEquPresets.put(name, equ);
        }
    }
    
    /* Сохраняет настройки эквалайзера. */
    public void saveEffects()
    {
        INIFile f = new INIFile();
        INIFile.Section effects = f.section("Effects");
        
        effects.put("bands", String.valueOf(currentEqu.length));
        for(int k = 0; k < currentEqu.length; ++k)
            effects.put("band" + k, String.valueOf(currentEqu[k]));
        effects.put("bass", String.valueOf(currentBassLevel));
        effects.put("presets", String.valueOf(userEquPresets.size()));
        
        int i = 0;
        for(TreeMap.Entry<String, int[]> pair : userEquPresets.entrySet())
        {
            int[] bands = pair.getValue();
            INIFile.Section preset = f.section("Preset" + (i++));
            
            preset.put("name", pair.getKey());
            preset.put("bands", String.valueOf(bands.length));
            for(int k = 0; k < bands.length; ++k)
                preset.put("band" + k, String.valueOf(bands[k]));
        }
        
        try {   f.write(FILES_PREFIX + EFFECTS_FILE);   }
        catch(IOException ignored) {}
    }
}
