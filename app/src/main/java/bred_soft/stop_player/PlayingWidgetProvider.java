package bred_soft.stop_player;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import bred_soft.stop_player.player.StopPlayer;
import bred_soft.stop_player.storage.PlaybackStorage;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.utils.AutoBindService.autoPeekService;



@SuppressWarnings("WeakerAccess")
public class PlayingWidgetProvider extends AppWidgetProvider
{
    public static final String ACTION_PREV       = "bred_soft.stop_player.ActionPrev";
    public static final String ACTION_STOP       = "bred_soft.stop_player.ActionStop";
    public static final String ACTION_PLAY_PAUSE = "bred_soft.stop_player.ActionPlayPause";
    public static final String ACTION_NEXT       = "bred_soft.stop_player.ActionNext";
    public static final String ACTION_SET_INFO   = "bred_soft.stop_player.ActionSetInfo";
    public static final String ACTION_AUTOPLAY   = "bred_soft.stop_player.ActionAutoplay";
    
    protected ComponentName receiver_nm = null;
    
    
    protected void finalize() throws Throwable
    {
        receiver_nm = null; super.finalize();
    }
    
    
    @Override
    public void onUpdate(Context context_, AppWidgetManager AWM_, int[] widget_ids_)
    {
        StopPlayer SP = autoPeekService(this, context_, StopPlayer.class);
        if(SP != null)
        {
            SP.sendWidgetInfo(null); return;
        }
        
        PlaybackStorage Storage = new PlaybackStorage(context_);
        Item last_item = (Storage.currentPlaylist != null)?
                             Storage.currentPlaylist.get(Storage.currentPosition) : null;
        Storage.destroy();
        
        if(last_item == null) last_item = new Item();
        _setInfo(context_, last_item.artist, last_item.trackname, false);
    }
    
    @Override
    public void onReceive(Context context_, Intent intent_)
    {
        super.onReceive(context_, intent_);
        String action = intent_.getAction();
        
        if(action == null) return;
        if(! action.startsWith("bred_soft.stop_player.")) return;
        
        if(action.equals(ACTION_SET_INFO))
        {
            String artist = intent_.getStringExtra("artist");
            String trackname  = intent_.getStringExtra("trackname");
            boolean is_playing = intent_.getBooleanExtra("is_playing", false);
            
            _setInfo(context_, artist, trackname, is_playing);
            return;
        }
        
        StopPlayer SP = autoPeekService(this, context_, StopPlayer.class);
        if(SP == null) // Без запущенного приложения.
        {
            if(action.equals(ACTION_PLAY_PAUSE))
            {
                context_.startActivity(new Intent(context_, MainActivity.class)
                                       .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                action = ACTION_AUTOPLAY;
            }
            if(action.equals(ACTION_AUTOPLAY))
            {
                context_.sendBroadcast(new Intent(context_, PlayingWidgetProvider.class)
                                       .setAction(ACTION_AUTOPLAY));
            }
            return;
        }
        
        switch(action)
        {
        case ACTION_AUTOPLAY:   SP.Play();      break;
        case ACTION_PREV:       SP.Previous();  break;
        case ACTION_STOP:       SP.Stop();      break;
        case ACTION_PLAY_PAUSE: SP.PlayPause(); break;
        case ACTION_NEXT:       SP.Next();
        }
    }
    
    
    // -*- internal methods -*-
    
    protected void _setInfo(Context context_,
                            String artist_, String trackname_, boolean is_playing_)
    {
        RemoteViews views = new RemoteViews(context_.getPackageName(), R.layout.playing_widget);
        
        Intent activity = new Intent(context_, MainActivity.class);
        views.setOnClickPendingIntent(
            R.id.w_info_area, PendingIntent.getActivity(context_, 0, activity, 0)
        );
        
        __button_set_action(context_, views, R.id.w_button_prev, ACTION_PREV);
        __button_set_action(context_, views, R.id.w_button_stop, ACTION_STOP);
        __button_set_action(context_, views, R.id.w_button_playpause, ACTION_PLAY_PAUSE);
        __button_set_action(context_, views, R.id.w_button_next, ACTION_NEXT);
        
        if(is_playing_)
            views.setImageViewResource(R.id.w_button_playpause, R.drawable.ic_action_pause);
        else
            views.setImageViewResource(R.id.w_button_playpause, R.drawable.ic_action_play);
        
        if(artist_ == null) artist_ = context_.getString(R.string.empty_artist);
        if(trackname_  == null) trackname_  = context_.getString(R.string.empty_trackname);
        
        views.setTextViewText(R.id.w_text_artist,    artist_);
        views.setTextViewText(R.id.w_text_trackname, trackname_);
        
        // ---
        
        if(receiver_nm == null)
            receiver_nm = new ComponentName(context_.getPackageName(), getClass().getName());
        
        AppWidgetManager AWM = AppWidgetManager.getInstance(context_);
        int[] widget_ids = AWM.getAppWidgetIds(receiver_nm);
        
        AWM.updateAppWidget(widget_ids, views);
    }
    
    private void __button_set_action(Context context_, RemoteViews views_,
                                     int id_, String action_)
    {
        Intent intent = new Intent(context_, PlayingWidgetProvider.class)
                        .setAction(action_);
        views_.setOnClickPendingIntent(
            id_, PendingIntent.getBroadcast(context_, 0, intent, 0)
        );
    }
}
