package bred_soft.stop_player;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.android.openfiledialog.OpenFileDialog;
import bred_soft.stop_player.player.StopPlayer;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.stop_player.storage.MusicStorage.Playlist;
import static bred_soft.utils.AutoBindService.getService;



@SuppressWarnings("WeakerAccess")
public class TrackEditActivity extends Activity
    implements ServiceConnection
{
    public EditText edit_track_uri;
    public EditText edit_track_artist;
    public EditText edit_track_trackname;
    
    protected StopPlayer SP = null;
    protected Playlist _playlist = null;
    
    protected int playlist_id = -1;
    protected int track_id = -1;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.track_edit);
        
        edit_track_uri       = (EditText)findViewById(R.id.edit_track_uri);
        edit_track_artist    = (EditText)findViewById(R.id.edit_track_artist);
        edit_track_trackname = (EditText)findViewById(R.id.edit_track_trackname);
        
        playlist_id = getIntent().getIntExtra("playlist-id", -1);
        track_id = getIntent().getIntExtra("track-id", -1);
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        bindService(new Intent(this, StopPlayer.class), this, BIND_AUTO_CREATE);
    }
    
    @Override
    public void onPause()
    {
        onServiceDisconnected(null); unbindService(this);
        super.onPause();
    }
    
    @Override
    public void onServiceConnected(ComponentName name, IBinder binder_)
    {
        SP = getService(binder_);
        if(SP == null || _playlist != null) return;
        
        _playlist = SP.Storage.getPlaylist(playlist_id);
        if(_playlist == null)
        {
            Toast.makeText(this, R.string.no_playlist_error, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        Item item = _playlist.get(track_id);
        if(item != null)
        {
            edit_track_uri.setText(item.uri);
            edit_track_artist.setText(item.artist);
            edit_track_trackname.setText(item.trackname);
        }
    }
    
    @Override
    public void onServiceDisconnected(ComponentName name)
    {   SP = null;   }
    
    
    // -*- * -*-
    
    public void on_button_select(View v)
    {
        new OpenFileDialog(this)
            .setOpenDialogListener(new OpenFileDialog.OpenDialogListener()
        {
            @Override public void OnSelectedFile(String fname_)
            {
                if(fname_ == null || fname_.isEmpty()) return;
                
                edit_track_uri.setText(fname_);
                
                Item tmp_item = new Item(fname_);
                if(edit_track_artist.length() == 0)
                    edit_track_artist.setText(tmp_item.artist);
                if(edit_track_trackname.length() == 0)
                    edit_track_trackname.setText(tmp_item.trackname);
            }
        }).show();
    }
    
    public void on_button_save(View v)
    {
        if(SP == null) return;
        
        Item item = new Item(edit_track_uri.getText().toString());
        
        String artist = edit_track_artist.getText().toString();
        String trackname = edit_track_trackname.getText().toString();
        if(! artist.isEmpty()) item.artist = artist;
        if(! trackname.isEmpty()) item.trackname = trackname;
        
        if(0 <= track_id && track_id < _playlist.size())
            _playlist.set(track_id, item);
        else
            _playlist.add(item);
        
        if(! SP.Storage.setPlaylist(playlist_id, _playlist))
            Toast.makeText(this, R.string.pl_save_error, Toast.LENGTH_SHORT).show();
        finish();
    }
}
