package bred_soft.stop_player;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.SurfaceHolder;

import bred_soft.stop_player.player.PlayingFeedbackItem;

import static bred_soft.stop_player.storage.MusicStorage.Item;
import static bred_soft.stop_player.storage.MusicStorage.Playlist;
import static bred_soft.stop_player.PlaylistAdapter.getTimeString;



@SuppressWarnings("WeakerAccess")
public class MainActivityCallbacks extends PlayingFeedbackItem
    implements SurfaceHolder.Callback
{
    private MainActivity activity;
    private SurfaceHolder holder;
    
    public boolean is_scroll = true;
    private boolean is_draw = false;
    
    private Paint brush;
    private int spector_color, spector_bg_color;
    private int spector_col_start, spector_col_width, spector_col_step;
    private int spector_line_height, spector_line_step, spector_lines_count;
    private float spector_value_coef;
    
    public static final float SPECTOR_COL_WIDTH = 6.0f /*dp*/;
    public static final float SPECTOR_COL_INDENT = 2.0f /*dp*/;
    public static final float SPECTOR_LINE_HEIGHT = 2.0f /*dp*/;
    public static final float SPECTOR_LINE_INDENT = 4.0f /*dp*/;
    public static final float SPECTOR_VALUE_COEF = -0.2632f /*heights/dB*/;
    
    
    public MainActivityCallbacks(MainActivity activity_)
    {
        activity = activity_;
        holder = activity.image_spector.getHolder();
        if(holder != null) holder.addCallback(this);
        
        brush = new Paint(); brush.setStyle(Paint.Style.FILL); brush.setStrokeWidth(1.0f);
        spector_color = activity.getResources().getColor(R.color.spector);
        spector_bg_color = activity.getResources().getColor(R.color.info_bg);
    }
    
    @Override
    public void destroy()
    {
        if(holder != null) holder.removeCallback(this);
        holder = null; activity = null; brush = null;
        
        super.destroy();
    }
    
    
    // -*- Playing Callbacks -*-
    
    @Override
    public void onSetPlaylist(Playlist playlist_)
    {
        if(activity.playlist_adapter != null)
            activity.playlist_adapter.destroy();
        
        activity.playlist_adapter = new PlaylistAdapter(
            activity, activity, activity.list_playlist, playlist_, false /*for play*/
        );
        if(activity.menu_main_track_search != null)
            activity.menu_main_track_search.setOnQueryTextListener(activity.playlist_adapter);
        
        activity.is_playlist_saved = true;
    }
    
    @Override
    public void onSetTrack(int playlist_i_, Item info_)
    {
        if(activity.playlist_adapter != null)
            activity.playlist_adapter.select(playlist_i_);
        
        activity.text_artist.setText(info_.artist);
        activity.text_trackname.setText(info_.trackname);
        
        onSetDuration(info_.duration);
    }
    
    @Override
    public void onSetDuration(int duration_)
    {
        if(duration_ > 0)
        {
            activity.seek_time.setMax(duration_);
            activity.text_full_time.setText(getTimeString(duration_));
        }
        else
        {   activity.text_full_time.setText(R.string.empty_time);   }
    }
    
    @Override
    public void onStop()
    {
        activity.seek_time.setProgress(0); activity.seek_time.setEnabled(false);
        activity.text_cur_time.setText(R.string.empty_time);
        
        activity.button_playpause.setImageResource(R.drawable.ic_action_play);
        onSpectorUpdate(null);
    }
    
    @Override
    public void onPlay()
    {
        activity.seek_time.setEnabled(true);
        activity.button_playpause.setImageResource(R.drawable.ic_action_pause);
    }
    
    @Override
    public void onPause()
    {
        activity.button_playpause.setImageResource(R.drawable.ic_action_play);
    }
    
    @Override
    public void onTrackTimeUpdate(int time_)
    {
        if(time_ < 0)
        {
            activity.seek_time.setProgress(0);
            activity.seek_time.setEnabled(false);
        }
        else if(is_scroll)
        {
            activity.seek_time.setProgress(time_);
            activity.seek_time.setEnabled(true);
        }
        
        activity.text_cur_time.setText(getTimeString(time_));
    }
    
    @Override
    public void onSpectorUpdate(float[] spector_)
    {
        if(! is_draw || holder == null) return;
        
        Canvas canvas = null;
        try
        {
            canvas = holder.lockCanvas(null);
            if(canvas == null) return;
            
            canvas.drawColor(spector_bg_color);
            if(spector_ == null) return;
            
            brush.setColor(spector_color);
            float w = (float)canvas.getWidth(), h = (float)canvas.getHeight();
            float offset = spector_col_start;
            
            for(int i = 0; i < spector_.length; ++i, offset += spector_col_step)
            {
                float v = spector_[i] * spector_value_coef;
                canvas.drawRect(offset, v, offset + spector_col_width, h, brush);
            }
            
            brush.setColor(spector_bg_color);
            offset = h - spector_line_step;
            
            for(int i = 0; i < spector_lines_count; ++i, offset -= spector_line_step)
                canvas.drawRect(0.0f, offset, w, offset + spector_line_height, brush);
        }
        finally
        {
            if(canvas != null) holder.unlockCanvasAndPost(canvas);
        }
    }
    
    
    // -*- Surface Callbacks -*-
    
    @Override
    public void surfaceCreated(SurfaceHolder holder_)
    {   is_draw = true;   }
    
    @Override
    public void surfaceChanged(SurfaceHolder holder_, int f_, int w_, int h_)
    {
        DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
        
        float col_width = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, SPECTOR_COL_WIDTH, metrics
        );
        float col_indent = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, SPECTOR_COL_INDENT, metrics
        );
        float line_height = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, SPECTOR_LINE_HEIGHT, metrics
        );
        float line_indent = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, SPECTOR_LINE_INDENT, metrics
        );
        
        int cols_count = (int)(w_ / (col_width + col_indent));
        spector_col_step = w_ / cols_count;
        spector_col_width = (int)Math.ceil(col_width);
        spector_col_start = (w_ - spector_col_step * (cols_count - 1) - spector_col_width) / 2;
        
        spector_lines_count = (int)(h_ / (line_height + line_indent));
        spector_line_step = h_ / spector_lines_count;
        spector_line_height = (int)Math.ceil(line_height);
        
        spector_value_coef = h_ * SPECTOR_VALUE_COEF;
        
        setSpector(cols_count);
        onSpectorUpdate(null);
    }
    
    @Override
    public void surfaceDestroyed(SurfaceHolder holder_)
    {   is_draw = false;   }
}
