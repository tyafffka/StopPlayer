package bred_soft.stop_player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import bred_soft.stop_player.player.StopPlayer;

import static android.widget.SeekBar.OnSeekBarChangeListener;

import static bred_soft.utils.AutoBindService.getService;



@SuppressWarnings("WeakerAccess")
public class EffectsActivity extends Activity
    implements ServiceConnection
{
    public LinearLayout layout_equ_bands;
    public TextView     text_max_equ_level;
    public TextView     text_min_equ_level;
    public SeekBar      seek_bass_level;
    
    protected StopPlayer SP = null;
    protected String _saved_name = "";
    
    protected int _equ_level_offset = 0;
    protected boolean _is_inflated = false;
    protected boolean _is_saved = true;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.effects);
        
        layout_equ_bands   = (LinearLayout)findViewById(R.id.layout_equ_bands);
        text_max_equ_level = (TextView)    findViewById(R.id.text_max_equ_level);
        text_min_equ_level = (TextView)    findViewById(R.id.text_min_equ_level);
        seek_bass_level    = (SeekBar)     findViewById(R.id.seek_bass_level);
        
        seek_bass_level.setOnSeekBarChangeListener(null);
        _is_inflated = false;
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        bindService(new Intent(this, StopPlayer.class), this, BIND_AUTO_CREATE);
    }
    
    @Override
    public void onPause()
    {
        onServiceDisconnected(null); unbindService(this);
        super.onPause();
    }
    
    @Override
    public void onDestroy()
    {
        seek_bass_level.setOnSeekBarChangeListener(null);
        
        for(int i = 0;; ++i)
        {
            SeekBar band = (SeekBar)layout_equ_bands.findViewWithTag(i);
            if(band == null) break;
            band.setOnSeekBarChangeListener(null);
        }
        
        super.onDestroy();
    }
    
    @Override
    public void onServiceConnected(ComponentName name_, IBinder binder_)
    {
        SP = getService(binder_);
        if(SP == null) return;
        
        if(! _is_inflated)
        {
            LayoutInflater inflater = getLayoutInflater();
            int[] range = SP.getEqualizerRange();
            int[] freqs = SP.getEqualizerFrequencies();
            
            text_min_equ_level.setText((range[0] / 100) + "дБ");
            text_max_equ_level.setText((range[1] / 100) + "дБ");
            
            _equ_level_offset = range[0];
            int max = range[1] - range[0];
            
            for(int k = 0; k < freqs.length; ++k)
            {
                View band = inflater.inflate(R.layout.equ_bands_item, layout_equ_bands, false);
                if(band == null) continue;
                
                int freq = freqs[k] / 1000;
                ((TextView)band.findViewById(R.id.text_band_freq)).setText(
                    (freq > 1000)?(freq / 1000 + "к"):(freq + "")
                );
                SeekBar seek_band_level = (SeekBar)band.findViewById(R.id.seek_band_level);
                seek_band_level.setMax(max);
                seek_band_level.setTag(k);
                
                layout_equ_bands.addView(band);
            }
            
            _is_inflated = true;
        }
        
        int[] levels = SP.getEqualizerLevels();
        for(int k = 0; k < levels.length; ++k)
        {
            SeekBar band = (SeekBar)layout_equ_bands.findViewWithTag(k);
            if(band == null) continue;
            
            band.setOnSeekBarChangeListener(null);
            band.setProgress(levels[k] - _equ_level_offset);
            band.setOnSeekBarChangeListener(equ_bands);
        }
        
        seek_bass_level.setOnSeekBarChangeListener(null);
        seek_bass_level.setProgress(SP.getBassLevel());
        seek_bass_level.setOnSeekBarChangeListener(bass_band);
        
        _is_saved = true;
    }
    
    @Override
    public void onServiceDisconnected(ComponentName name_)
    {
        if(! _is_saved && SP != null)
        {
            SP.Storage.saveEffects(); _is_saved = true;
        }
        SP = null;
    }
    
    
    // -*- GUI handlers -*-
    
    public void on_button_equ_preset(View v)
    {
        startActivity(new Intent(this, PresetsActivity.class));
    }
    
    public void on_button_save_equ(View v)
    {
        if(SP == null) return;
        
        View dialog = getLayoutInflater().inflate(R.layout.preset_name_dialog, null);
        final EditText edit_name = (EditText)dialog.findViewById(R.id.edit_preset_name);
        
        edit_name.setText(_saved_name);
        
        new AlertDialog.Builder(this)
            .setCancelable(true).setView(dialog)
            .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                String new_name = edit_name.getText().toString();
                if(! new_name.isEmpty()) _saved_name = new_name;
                
                SP.saveEqualizerPreset(new_name);
                _is_saved = true;
            }
        }).show();
    }
    
    protected OnSeekBarChangeListener equ_bands = new OnSeekBarChangeListener()
    { /* on_seek_BAND_level_change */
        @Override
        public void onProgressChanged(SeekBar v, int progress, boolean fromUser)
        {
            if(SP != null)
            {
                SP.setEqualizerLevel((short)(int)(Integer)v.getTag(),
                                     progress + _equ_level_offset);
                _is_saved = false;
            }
        }
        
        @Override
        public void onStartTrackingTouch(SeekBar v) { /* holded */ }
        
        @Override
        public void onStopTrackingTouch(SeekBar v) { /* holded */ }
    };
    
    protected OnSeekBarChangeListener bass_band = new OnSeekBarChangeListener()
    { /* on_seek_BASS_level_change */
        @Override
        public void onProgressChanged(SeekBar v, int progress, boolean fromUser)
        {
            if(SP != null)
            {
                SP.setBassLevel(progress); _is_saved = false;
            }
        }
        
        @Override
        public void onStartTrackingTouch(SeekBar v) { /* holded */ }
        
        @Override
        public void onStopTrackingTouch(SeekBar v) { /* holded */ }
    };
}
