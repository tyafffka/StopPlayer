package bred_soft.stop_player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

//import com.mobeta.android.dslv.DragSortListView;
import bred_soft.stop_player.player.StopPlayer;

import static bred_soft.stop_player.storage.MusicStorage.Playlist;
import static bred_soft.stop_player.PlaylistAdapter.ClickListener;
import static bred_soft.utils.AutoBindService.getService;



@SuppressWarnings("WeakerAccess")
public class PlaylistActivity extends Activity
    implements ServiceConnection, ClickListener
{
    public SearchView menu_track_search = null;
    protected ListView list_playlist_view;
    
    protected StopPlayer SP = null;
    protected PlaylistAdapter playlist_adapter = null;
    
    protected int playlist_id = -1;
    protected int selected_track = -1;
    protected boolean _is_saved = true;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState_)
    {
        super.onCreate(savedInstanceState_);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.playlist_view);
        
        list_playlist_view = (ListView)findViewById(R.id.list_playlist_view);
        
        playlist_id = getIntent().getIntExtra("playlist-id", -1);
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        bindService(new Intent(this, StopPlayer.class), this, BIND_AUTO_CREATE);
    }
    
    @Override
    protected void onPause()
    {
        onServiceDisconnected(null); unbindService(this);
        super.onPause();
    }
    
    @Override
    protected void onDestroy()
    {
        list_playlist_view.setOnItemClickListener(null);
        list_playlist_view = null;
        menu_track_search = null;
        super.onDestroy();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu_)
    {
        getMenuInflater().inflate(R.menu.playlist_view, menu_);
        
        menu_track_search = (SearchView)menu_.findItem(R.id.menu_track_search)
                                             .getActionView();
        if(playlist_adapter != null)
            menu_track_search.setOnQueryTextListener(playlist_adapter);
        
        return super.onCreateOptionsMenu(menu_);
    }
    
    @Override
    public void onServiceConnected(ComponentName name_, IBinder binder_)
    {
        SP = getService(binder_);
        if(SP == null) return;
        
        Playlist playlist = SP.Storage.getPlaylist(playlist_id);
        if(playlist == null)
        {
            Toast.makeText(this, R.string.no_playlist_error, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        if(playlist_adapter != null) playlist_adapter.destroy();
        _is_saved = true;
        
        playlist_adapter = new PlaylistAdapter(
            this, this, list_playlist_view, playlist, true /*for view*/
        );
        if(menu_track_search != null)
            menu_track_search.setOnQueryTextListener(playlist_adapter);
        
        setTitle(SP.Storage.getPlaylistName(playlist_id));
    }
    
    @Override
    public void onServiceDisconnected(ComponentName name_)
    {
        if(! _is_saved && SP != null &&
           playlist_id >= 0 && playlist_adapter != null)
        {
            if(! SP.Storage.setPlaylist(playlist_id, playlist_adapter.getPlaylist()))
                Toast.makeText(this, R.string.pl_save_error, Toast.LENGTH_SHORT).show();
        }
        _is_saved = true;
        SP = null;
        
        if(playlist_adapter != null)
        {
            if(menu_track_search != null)
                menu_track_search.setOnQueryTextListener(null);
            
            playlist_adapter.destroy(); playlist_adapter = null;
        }
    }
    
    // ---
    
    @Override
    public void onItemSkip(long id_)
    {
        selected_track = -1;
        
        playlist_adapter.getPlaylist().flipSkip((int)id_);
        playlist_adapter.notifyDataSetChanged();
        _is_saved = false;
    }
    
    @Override
    public void onItemClick(long id_)
    {
        selected_track = -1;
        
        if(SP.start(playlist_adapter.getPlaylist().copy(), (int)id_))
        {
            startActivity(new Intent(this, MainActivity.class)
                          .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }
    
    @Override
    public void onItemDoc(long id_)
    {
        selected_track = (int)id_;
        
        new AlertDialog.Builder(this)
            .setCancelable(true)
            .setTitle(playlist_adapter.getItem(selected_track).trackname)
            .setItems(R.array.track_dialog, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                dialog_.dismiss();
                switch(which_)
                {
                case 0: onClickEdit();    return;
                case 1: onClickGetCopy(); return;
                case 2: onClickRemove();
                }
            }
        }).show();
    }
    
    
    // --- Actions --- //
    
    public void onMenuPlay(MenuItem m)
    {
        selected_track = -1;
        if(SP.start(playlist_adapter.getPlaylist().copy()))
        {
            startActivity(new Intent(this, MainActivity.class)
                          .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }
    
    public void onMenuAdd(MenuItem m)
    {
        selected_track = -1;
        startActivity(new Intent(this, TrackEditActivity.class)
                      .putExtra("playlist-id", playlist_id)
                      .putExtra("track-id", -1));
    }
    
    public void onMenuPaste(MenuItem m)
    {
        selected_track = -1;
        if(SP.Storage.clipboardTrack == null)
        {
            Toast.makeText(this, R.string.paste_error, Toast.LENGTH_SHORT).show();
            return;
        }
        
        playlist_adapter.getPlaylist().add(-1, SP.Storage.clipboardTrack.copy());
        playlist_adapter.notifyDataSetChanged();
        _is_saved = false;
    }
    
    public void onMenuRename(MenuItem m)
    {
        selected_track = -1;
        
        ListOfPlaylistsActivity._playlistNameDialog(
            this, R.string.playlist_rename,
            SP.Storage.getPlaylistName(playlist_id),
            new ListOfPlaylistsActivity._PlaylistNameCallback()
        {
            @Override public void set(String new_name_)
            {
                int new_id = SP.Storage.renamePlaylist(playlist_id, new_name_);
                if(new_id < 0)
                    Toast.makeText(PlaylistActivity.this,
                                   R.string.pl_name_error, Toast.LENGTH_SHORT).show();
                else
                {
                    playlist_id = new_id; setTitle(new_name_);
                }
            }
        });
    }
    
    public void onMenuCopy(MenuItem m)
    {
        selected_track = -1;
        
        ListOfPlaylistsActivity._playlistNameDialog(
            this, R.string.playlist_copy,
            SP.Storage.getPlaylistName(playlist_id),
            new ListOfPlaylistsActivity._PlaylistNameCallback()
        {
            @Override public void set(String new_name_)
            {
                int pl_id = SP.Storage.copyPlaylist(playlist_id, new_name_);
                if(pl_id < 0)
                    Toast.makeText(PlaylistActivity.this,
                                   R.string.pl_save_error, Toast.LENGTH_SHORT).show();
                else
                {
                    startActivity(new Intent(PlaylistActivity.this, PlaylistActivity.class)
                                  .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME)
                                  .putExtra("playlist-id", pl_id));
                    finish();
                }
            }
        });
    }
    
    public void onMenuMerge(MenuItem m)
    {
        selected_track = -1;
        
        new AlertDialog.Builder(this)
            .setCancelable(true).setTitle(R.string.playlist_merge)
            .setItems(SP.Storage.getPlaylistsNames(), new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                Playlist pl_from = SP.Storage.getPlaylist(which_);
                if(pl_from == null) return;
                
                playlist_adapter.getPlaylist().addAll(pl_from);
                playlist_adapter.notifyDataSetChanged();
                _is_saved = false;
            }
        }).show();
    }
    
    public void onMenuRemove(MenuItem m)
    {
        new AlertDialog.Builder(this)
            .setCancelable(true).setMessage(getString(R.string.playlist_remove_message,
                                                      SP.Storage.getPlaylistName(playlist_id)))
            .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                SP.Storage.removePlaylist(playlist_id);
                
                selected_track = -1; playlist_id = -1;
                finish();
            }
        }).show();
    }
    
    // ---
    
    public void onClickEdit()
    {
        startActivity(new Intent(this, TrackEditActivity.class)
                      .putExtra("playlist-id", playlist_id)
                      .putExtra("track-id", selected_track));
        selected_track = -1;
    }
    
    public void onClickGetCopy()
    {
        SP.Storage.clipboardTrack = playlist_adapter.getItem(selected_track).copy();
        selected_track = -1;
    }
    
    public void onClickRemove()
    {
        new AlertDialog.Builder(this)
            .setCancelable(true).setMessage(getString(R.string.track_remove_message,
                                                      playlist_adapter.getItem(selected_track).trackname))
            .setNegativeButton(R.string.cancel, null)
            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
        {
            @Override public void onClick(DialogInterface dialog_, int which_)
            {
                playlist_adapter.getPlaylist().remove(selected_track);
                playlist_adapter.notifyDataSetChanged();
                
                selected_track = -1; _is_saved = false;
            }
        }).show();
    }
}
