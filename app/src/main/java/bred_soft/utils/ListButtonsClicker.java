package bred_soft.utils;

import android.view.View;



public class ListButtonsClicker implements View.OnClickListener
{
    protected static class _Ident
    {
        public int position;
        public short tag;
    }
    
    public interface OnListButtonClickListener
    {
        void onListButtonClick(int position_, short tag_);
    }
    
    // -*-
    
    private OnListButtonClickListener __listener;
    
    
    public ListButtonsClicker(OnListButtonClickListener listener_)
    {   __listener = listener_;   }
    
    public void destroy()
    {   __listener = null;   }
    
    public void setOnClick(View v, int position_, short tag_)
    {
        _Ident ident = new _Ident();
        ident.position = position_;
        ident.tag = tag_;
        
        v.setTag(ident);
        v.setOnClickListener(this);
    }
    
    @Override
    public void onClick(View v)
    {
        _Ident ident = (_Ident)v.getTag();
        __listener.onListButtonClick(ident.position, ident.tag);
    }
}
