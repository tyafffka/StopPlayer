package bred_soft.utils;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;



public abstract class AutoBindService extends Service
{
    private static class AutoBinder extends Binder
    {
        AutoBindService service;
        
        AutoBinder(AutoBindService service_)
        {   service = service_;   }
        
        protected void finalize() throws Throwable
        {
            service = null; super.finalize();
        }
    }
    
    
    @Override
    public IBinder onBind(Intent intent)
    {
        return new AutoBinder(this);
    }
    
    
    @SuppressWarnings("unchecked")
    public static <C extends AutoBindService> C getService(IBinder binder_)
    {
        if(!(binder_ instanceof AutoBinder)) return null;
        return (C)(((AutoBinder)binder_).service);
    }
    
    public static <C extends AutoBindService>
    C autoPeekService(BroadcastReceiver receiver_, Context context_, Class<? extends C> class_)
    {
        return getService(receiver_.peekService(
            context_, new Intent(context_, class_)
        ));
    }
}
