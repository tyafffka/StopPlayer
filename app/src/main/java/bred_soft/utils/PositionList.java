package bred_soft.utils;



@SuppressWarnings("WeakerAccess")
public class PositionList<T>
{
    private static class Link<T>
    {
        T item; // if null - it's a voidLink
        Link<T> prev, next;
        int position; // for voidLink it's a size
    }
    
    public static class ListIterator<T>
    {
        private Link<T> link;
        
        private ListIterator(Link<T> link_)
        {   link = link_;   }
        
        public ListIterator(ListIterator<T> cp)
        {   link = cp.link;   }
        
        @Override
        public boolean equals(Object o)
        {   return(o instanceof ListIterator && ((ListIterator)o).link == link);   }
        
        @Override
        public int hashCode()
        {   return link.hashCode();   }
        
        
        public void next()
        {   link = link.next;   }
        
        public void prev()
        {   link = link.prev;   }
        
        public boolean isEnd()
        {   return(link.item == null);   }
        
        public T get()
        {   return link.item;   }
        
        public void set(T item_)
        {
            if(item_ == null || link.item == null) return;
            link.item = item_;
        }
        
        public int position()
        {   return link.position;   }
    } //- class ListIterator<T>
    
    
    // -*-
    
    private Link<T> voidLink;
    
    
    public PositionList()
    {
        voidLink = new Link<T>();
        
        voidLink.item = null;
        voidLink.prev = voidLink.next = voidLink;
        voidLink.position = 0;
    }
    
    public void destroy()
    {
        clear();
        voidLink.prev = voidLink.next = null;
        voidLink = null;
    }
    
    public void clear()
    {
        Link<T> link = voidLink.next;
        while(link.item != null) // or (link == voidLink)
        {
            Link<T> next_link = link.next;
            
            link.item = null;
            link.next = link.prev = null;
            link.position = 0;
            
            link = next_link;
        }
        
        voidLink.prev = voidLink.next = voidLink;
        voidLink.position = 0;
    }
    
    public int size()
    {   return voidLink.position;   }
    
    public boolean isEmpty()
    {   return(voidLink.position == 0);   }
    
    public ListIterator<T> iterator(int position_)
    {
        if(position_ < 0 || voidLink.position <= position_)
            return new ListIterator<T>(voidLink);
        
        Link<T> link = voidLink;
        if(position_ < voidLink.position / 2)
        {
            do {   link = link.next;   }
            while(link.position < position_);
        }
        else
        {
            do {   link = link.prev;   }
            while(link.position > position_);
        }
        return new ListIterator<T>(link);
    }
    
    public T get(int position_)
    {   return iterator(position_).get();   }
    
    public void set(int position_, T item_)
    {
        ListIterator<T> it = iterator(position_);
        if(it.isEnd()) add(it, item_);
        else it.set(item_);
    }
    
    public void add(ListIterator<T> before_, T item_)
    {
        if(item_ == null) return;
        
        Link<T> link = before_.link, new_link = new Link<T>();
        new_link.item = item_;
        
        new_link.prev = link.prev;
        new_link.prev.next = new_link;
        new_link.next = link;
        new_link.next.prev = new_link;
        
        new_link.position = link.position;
        for(;;)
        {
            ++link.position;
            if(link.item == null) return; // or (link == voidLink)
            link = link.next;
        }
    }
    
    public void add(int before_, T item_)
    {   add(iterator(before_), item_);   }
    
    public void add(T item_)
    {   add(iterator(-1), item_);   }
    
    public void addAll(PositionList<T> other_)
    {
        for(Link<T> other_link = other_.voidLink.next;
            other_link.item != null; other_link = other_link.next) // or (other_link == other_.voidLink)
        {
            Link<T> new_link = new Link<T>();
            new_link.item = other_link.item;
            
            new_link.prev = voidLink.prev;
            new_link.prev.next = new_link;
            new_link.next = voidLink;
            new_link.next.prev = new_link;
            
            new_link.position = voidLink.position;
            ++voidLink.position;
        }
    }
    
    public void remove(ListIterator<T> position_)
    {
        Link<T> link = position_.link;
        position_.link = voidLink;
        
        if(link.item == null) return; // or (link == voidLink)
        
        link.item = null;
        link.prev.next = link.next;
        link.next.prev = link.prev;
        
        do
        {
            link = link.next; --link.position;
        }
        while(link.item != null); // or (link != voidLink)
    }
    
    public void remove(int position_)
    {   remove(iterator(position_));   }
    
    public void move(ListIterator<T> from_, ListIterator<T> before_)
    {
        if(from_.isEnd() || from_.link == before_.link) return;
        
        Link<T> link = from_.link, to_link = before_.link;
        
        if(link.position < to_link.position)
            for(;;)
            {
                link = link.next;
                if(link == to_link) break;
                --link.position;
            }
        else
            do
            {
                link = link.prev; ++link.position;
            }
            while(link != to_link);
        
        link = from_.link;
        link.position = to_link.position - 1;
        
        link.prev.next = link.next;
        link.next.prev = link.prev;
        
        link.prev = to_link.prev;
        link.prev.next = link;
        link.next = to_link;
        link.next.prev = link;
    }
    
    public void move(int from_, int before_)
    {
        if(from_ == before_) return;
        move(iterator(from_), iterator(before_));
    }
}
