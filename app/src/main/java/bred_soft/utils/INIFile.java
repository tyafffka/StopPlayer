package bred_soft.utils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



@SuppressWarnings("WeakerAccess")
public class INIFile
{
    public class Section extends TreeMap<String, String>
    {
        public String get(String key_, String default_)
        {
            String r = get(key_); return (r != null)? r : default_;
        }
    }
    
    //private static Pattern __re_comment = Pattern.compile("^\\s*[;#]");
    private static Pattern __re_section = Pattern.compile("^\\s*\\[(.+?)\\]\\s*$");
    private static Pattern __re_value = Pattern.compile("^\\s*(.*?\\S)\\s*=\\s*(.*\\S|)\\s*$");
    
    protected TreeMap<String, Section> _sections;
    
    
    public INIFile()
    {
        _sections = new TreeMap<>();
        _sections.put("", new Section());
    }
    
    public INIFile(String filename_) throws IOException
    {
        this(); read(filename_);
    }
    
    public void destroy()
    {
        for(TreeMap m : _sections.values()) m.clear();
        _sections.clear();
    }
    
    
    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public void read(String filename_) throws IOException
    {
        BufferedReader in = new BufferedReader(new InputStreamReader(
            new FileInputStream(filename_), Charset.forName("UTF-8")
        ));
        try
        {
            String line; Matcher match;
            Section section = _sections.get("");
            
            while((line = in.readLine()) != null)
            {
                if((match = __re_section.matcher(line)).matches())
                {
                    line = match.group(1);
                    if((section = _sections.get(line)) == null)
                        _sections.put(line, (section = new Section()));
                }
                else if((match = __re_value.matcher(line)).matches())
                {
                    section.put(match.group(1), match.group(2));
                }
            }
        }
        finally
        {   in.close();   }
    }
    
    private static void __write_section(BufferedWriter out_, Map<String, String> section_)
        throws IOException
    {
        for(Map.Entry<String, String> pair : section_.entrySet())
        {
            if(pair.getKey().isEmpty()) continue;
            
            out_.write(pair.getKey()); out_.write("="); out_.write(pair.getValue());
            out_.newLine(); out_.flush();
        }
    }
    
    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public void write(String filename_) throws IOException
    {
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(filename_), Charset.forName("UTF-8")
        ));
        try
        {
            __write_section(out, _sections.get(""));
            
            for(Map.Entry<String, Section> pair : _sections.entrySet())
            {
                if(pair.getKey().isEmpty()) continue;
                
                out.newLine(); out.write("["); out.write(pair.getKey()); out.write("]");
                out.newLine(); out.flush();
                
                __write_section(out, pair.getValue());
            }
        }
        finally
        {   out.close();   }
    }
    
    
    public Section defaultSection() {   return _sections.get("");   }
    
    public Section section(String sname_)
    {
        Section r = _sections.get(sname_);
        if(r == null) _sections.put(sname_, (r = new Section()));
        return r;
    }
}
