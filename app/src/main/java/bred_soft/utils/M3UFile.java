package bred_soft.utils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bred_soft.stop_player.storage.MusicStorage;



public class M3UFile
{
    private static Pattern __re_info = Pattern.compile("^\\s*#EXTINF:\\s*((\\d+)\\s*,\\s*)?((.+?)\\s+-\\s+)?(.*\\S)\\s*$");
    private static Pattern __re_uri = Pattern.compile("^\\s*(?!#)(.*\\S)\\s*$");
    
    
    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public static MusicStorage.Playlist read(String filename_) throws IOException
    {
        final String coding = filename_.toLowerCase().endsWith(".m3u") ?
                              "windows-1252" : "UTF-8";
        BufferedReader in = new BufferedReader(new InputStreamReader(
            new FileInputStream(filename_), Charset.forName(coding)
        ));
        try
        {
            String line; Matcher match;
            MusicStorage.Playlist pl = new MusicStorage.Playlist();
            MusicStorage.Item item = null;
            
            while((line = in.readLine()) != null)
            {
                if((match = __re_info.matcher(line)).matches())
                {
                    String duration = match.group(2);
                    String artist = match.group(4);
                    String trackname = match.group(5);
                    
                    item = new MusicStorage.Item();
                    item.artist = (artist != null)? artist : MusicStorage.EMPTY_ARTIST;
                    item.trackname = trackname;
                    item.duration = (duration != null)? Integer.decode(duration) : -1;
                    item.skip = false;
                }
                else if((match = __re_uri.matcher(line)).matches())
                {
                    if(item == null)
                    {
                        item = new MusicStorage.Item();
                        item.artist = MusicStorage.EMPTY_ARTIST;
                        item.trackname = MusicStorage.EMPTY_TRACKNAME;
                        item.duration = -1;
                        item.skip = false;
                    }
                    item.uri = match.group(1);
                    
                    pl.add(item); item = null;
                }
            }
            return pl;
        }
        finally
        {   in.close();   }
    }
    
    
    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public static void write(String filename_, MusicStorage.Playlist pl_) throws IOException
    {
        final String coding = filename_.toLowerCase().endsWith(".m3u") ?
                              "windows-1252" : "UTF-8";
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(filename_), Charset.forName(coding)
        ));
        try
        {
            out.write("#EXTM3U"); out.newLine(); out.flush();
            
            for(PositionList.ListIterator<MusicStorage.Item> it = pl_.iterator(0);
                (! it.isEnd()); it.next())
            {
                MusicStorage.Item item = it.get();
                
                out.newLine();
                out.write("#EXTINF:"); out.write(String.valueOf(item.duration));
                out.write(", "); out.write(item.artist);
                out.write(" - "); out.write(item.trackname);
                out.newLine(); out.write(item.uri);
                out.newLine(); out.flush();
            }
        }
        finally
        {   out.close();   }
    }
}
