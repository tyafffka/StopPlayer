/*
The MIT License (MIT)

Copyright (c) 2013-2014 Sergey Antonov
Copyright (c) 2017-2019 ТяФ/ка (tyafffka)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package edu.android.openfiledialog;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;

import bred_soft.stop_player.R;



@SuppressWarnings("WeakerAccess")
public class OpenFileDialog extends AlertDialog.Builder
{
    public interface OpenDialogListener
    {
        void OnSelectedFile(String fileName);
    }
    
    private TextView title;
    private ListView listView;
    
    private static String lastPath = null;
    
    private OpenDialogListener listener = null;
    private FilenameFilter filenameFilter = null;
    private boolean isDirectoryMode = false;
    private String currentPath;
    private List<File> files = new ArrayList<File>();
    private int selectedIndex = -1;
    
    
    private class FileAdapter extends ArrayAdapter<File>
    {
        public FileAdapter(Context context, List<File> files)
        {
            super(context, android.R.layout.simple_list_item_1, files);
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            TextView view = (TextView)super.getView(position, convertView, parent);
            File file = getItem(position);
            
            if(file != null)
            {
                if(selectedIndex == position)
                    view.setBackgroundResource(R.color.search_hili);
                else
                    view.setBackgroundResource(R.color.transparent);
                
                if(file.isDirectory())
                {
                    view.setText(file.getName() + "/");
                    view.setPadding(50, 0, 0, 0);
                }
                else
                {
                    view.setText(file.getName());
                    view.setPadding(20, 0, 0, 0);
                }
            }
            return view;
        }
    } //- class FileAdapter
    
    
    //-{ API
    public OpenFileDialog(Context context)
    {
        super(context);
        if(lastPath == null)
            lastPath = Environment.getExternalStorageDirectory().getPath();
        currentPath = lastPath;
        
        title = createTitle(context);
        changeTitle();
        
        LinearLayout linearLayout = createMainLayout(context);
        linearLayout.addView(createBackItem(context));
        listView = createListView(context);
        linearLayout.addView(listView);
        
        setCustomTitle(title);
        setView(linearLayout);
        setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                lastPath = currentPath;
                if(listener == null) return;
                
                if(isDirectoryMode)
                    listener.OnSelectedFile(currentPath);
                else if(selectedIndex >= 0)
                    listener.OnSelectedFile(listView.getItemAtPosition(selectedIndex).toString());
            }
        });
        setNegativeButton(R.string.cancel, null);
    }
    
    @Override
    public AlertDialog show()
    {
        files.addAll(getFiles(currentPath));
        listView.setAdapter(new FileAdapter(getContext(), files));
        return super.show();
    }
    
    public OpenFileDialog setOpenDialogListener(OpenDialogListener listener)
    {
        this.listener = listener; return this;
    }
    
    public OpenFileDialog setFilter(final String filter)
    {
        filenameFilter = new FilenameFilter()
        {
            @Override
            public boolean accept(File file, String fileName)
            {
                File tempFile = new File(String.format("%s/%s", file.getPath(), fileName));
                if(tempFile.isFile()) return tempFile.getName().matches(filter);
                return true;
            }
        };
        return this;
    }
    
    public OpenFileDialog setDirectoryMode()
    {
        isDirectoryMode = true; return this;
    }
    
    public OpenFileDialog setCurrentPath(String path_)
    {
        currentPath = path_; return this;
    }
    //}- API
    
    private static Display getDefaultDisplay(Context context)
    {
        return ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    }
    
    private static Point getScreenSize(Context context)
    {
        Point screeSize = new Point();
        getDefaultDisplay(context).getSize(screeSize);
        return screeSize;
    }
    
    private static int getLinearLayoutMinHeight(Context context)
    {
        return getScreenSize(context).y;
    }
    
    private LinearLayout createMainLayout(Context context)
    {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setMinimumHeight(getLinearLayoutMinHeight(context));
        return linearLayout;
    }
    
    private int getItemHeight(Context context)
    {
        TypedValue value = new TypedValue();
        DisplayMetrics metrics = new DisplayMetrics();
        context.getTheme().resolveAttribute(android.R.attr.listPreferredItemHeightSmall, value, true);
        
        getDefaultDisplay(context).getMetrics(metrics);
        return (int)TypedValue.complexToDimension(value.data, metrics);
    }
    
    private TextView createTextView(Context context, int style)
    {
        TextView textView = new TextView(context);
        textView.setTextAppearance(context, style);
        int itemHeight = getItemHeight(context);
        textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, itemHeight));
        textView.setMinHeight(itemHeight);
        return textView;
    }
    
    private TextView createTitle(Context context)
    {
        TextView textView = createTextView(
            context, android.R.style.TextAppearance_Holo_DialogWindowTitle
        );
        
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(15, 0, 0, 0);
        return textView;
    }
    
    private TextView createBackItem(Context context)
    {
        TextView textView = createTextView(
            context, android.R.style.TextAppearance_Holo_Medium
        );
        textView.setText("< Назад >");
        
        textView.setLayoutParams(new ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(0, 0, 0, 0);
        
        textView.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View view)
            {
                File file = new File(currentPath);
                File parentDirectory = file.getParentFile();
                if(parentDirectory != null)
                {
                    currentPath = parentDirectory.getPath();
                    RebuildFiles(((FileAdapter) listView.getAdapter()));
                }
            }
        });
        return textView;
    }
    
    public int getTextWidth(String text, Paint paint)
    {
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return bounds.left + bounds.width() + 80;
    }
    
    private void changeTitle()
    {
        String titleText = currentPath;
        int screenWidth = getScreenSize(getContext()).x;
        int maxWidth = (int)(screenWidth * 0.99);
        
        if(getTextWidth(titleText, title.getPaint()) > maxWidth)
        {
            while(getTextWidth("..." + titleText, title.getPaint()) > maxWidth)
            {
                int start = titleText.indexOf("/", 2);
                if(start > 0)
                    titleText = titleText.substring(start);
                else
                    titleText = titleText.substring(2);
            }
            title.setText("..." + titleText);
        }
        else
        {   title.setText(titleText);   }
    }
    
    private List<File> getFiles(String directoryPath)
    {
        File directory = new File(directoryPath);
        File[] list = directory.listFiles(filenameFilter);
        Log.i("OpenFileDialog", "full list = " + ((list == null)? "null" : String.valueOf(list.length)) + ";");
        
        if(list == null) list = new File[0];
        List<File> fileList = new ArrayList<>();
        for(File fp : list) if(! isDirectoryMode || fp.isDirectory()) fileList.add(fp);
        
        Collections.sort(fileList, new Comparator<File>()
        {
            @Override public int compare(File file, File file2)
            {
                if(! isDirectoryMode)
                {
                    if(file.isDirectory() && file2.isFile()) return -1;
                    if(file.isFile() && file2.isDirectory()) return 1;
                }
                return file.getPath().compareTo(file2.getPath());
            }
        });
        return fileList;
    }
    
    private void RebuildFiles(ArrayAdapter<File> adapter)
    {
        try
        {
            List<File> fileList = getFiles(currentPath);
            files.clear();
            selectedIndex = -1;
            files.addAll(fileList);
            adapter.notifyDataSetChanged();
            changeTitle();
        }
        catch(NullPointerException e)
        {
            Toast.makeText(getContext(), "Ошибка: доступ запрещён!", Toast.LENGTH_SHORT).show();
        }
    }
    
    private ListView createListView(Context context)
    {
        ListView listView = new ListView(context);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l)
            {
                final ArrayAdapter<File> adapter = (FileAdapter)adapterView.getAdapter();
                File file = adapter.getItem(index);
                if(file == null) return;
                
                if(file.isDirectory())
                {
                    currentPath = file.getPath();
                    RebuildFiles(adapter);
                }
                else
                {
                    if(index != selectedIndex) selectedIndex = index;
                    else selectedIndex = -1;
                    
                    adapter.notifyDataSetChanged();
                }
            }
        });
        return listView;
    }
}
